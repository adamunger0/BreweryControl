EESchema Schematic File Version 4
LIBS:RelayDriver-cache
EELAYER 26 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "Relay driver and GPIO connections board."
Date "2018-10-06"
Rev "2"
Comp ""
Comment1 "This is a relay driver and power distribution board to interface a RPi 3B+ to 6 SSR's."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5BB9A2C3
P 4750 2350
F 0 "J5" H 4830 2342 50  0000 L CNN
F 1 "SSR Elem 1" H 4830 2251 50  0000 L CNN
F 2 "libraries:TEConnectivity_4pos_90deg" H 4750 2350 50  0001 C CNN
F 3 "~" H 4750 2350 50  0001 C CNN
F 4 "A108794CT-ND" H 4750 2350 50  0001 C CNN "Digikey Part Number"
	1    4750 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 5BB9A38D
P 4750 4550
F 0 "J6" H 4850 4550 50  0000 L CNN
F 1 "SSR Elem 2" H 4850 4450 50  0000 L CNN
F 2 "libraries:TEConnectivity_4pos_90deg" H 4750 4550 50  0001 C CNN
F 3 "~" H 4750 4550 50  0001 C CNN
F 4 "A108794CT-ND" H 4750 4550 50  0001 C CNN "Digi-Key Part Number"
	1    4750 4550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5BB9A4B8
P 1650 5850
F 0 "J2" V 1800 5800 50  0000 L CNN
F 1 "Temp 2" V 1900 5700 50  0000 L CNN
F 2 "libraries:TEConnectivity_3pos_90deg" H 1650 5850 50  0001 C CNN
F 3 "~" H 1650 5850 50  0001 C CNN
F 4 "A100890CT-ND" V 1650 5850 50  0001 C CNN "Digi-Key Part Number"
	1    1650 5850
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5BB9A54A
P 1050 5850
F 0 "J1" V 1200 5800 50  0000 L CNN
F 1 "Temp 3" V 1300 5700 50  0000 L CNN
F 2 "libraries:TEConnectivity_3pos_90deg" H 1050 5850 50  0001 C CNN
F 3 "~" H 1050 5850 50  0001 C CNN
F 4 "A100890CT-ND" V 1050 5850 50  0001 C CNN "Digi-Key Part Number"
	1    1050 5850
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 5BB9A5FD
P 2150 5850
F 0 "J4" V 2300 5800 50  0000 L CNN
F 1 "Temp 1" V 2400 5700 50  0000 L CNN
F 2 "libraries:TEConnectivity_3pos_90deg" H 2150 5850 50  0001 C CNN
F 3 "~" H 2150 5850 50  0001 C CNN
F 4 "A100890CT-ND" V 2150 5850 50  0001 C CNN "Digi-Key Part Number"
	1    2150 5850
	0    1    1    0   
$EndComp
$Comp
L Connector:USB_A J10
U 1 1 5BB9AA70
P 9700 5550
F 0 "J10" V 10100 5250 50  0000 C CNN
F 1 "RPi Pout" V 10000 5200 50  0000 C CNN
F 2 "libraries:Tensility-USB_A" H 9850 5500 50  0001 C CNN
F 3 " ~" H 9850 5500 50  0001 C CNN
F 4 "54-00003-ND" V 9700 5550 50  0001 C CNN "Digi-Key Part Number"
	1    9700 5550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5BB9E9B2
P 3900 3100
F 0 "R4" V 3693 3100 50  0000 C CNN
F 1 "0-330" V 3784 3100 50  0000 C CNN
F 2 "digikey-footprints:0805" V 3830 3100 50  0001 C CNN
F 3 "~" H 3900 3100 50  0001 C CNN
F 4 "‎A129233CT-ND‎, P330ADCT-ND‎" V 3900 3100 50  0001 C CNN "Digi-Key Part Number"
	1    3900 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5BB9EA9B
P 3900 5200
F 0 "R5" V 3693 5200 50  0000 C CNN
F 1 "0-330" V 3784 5200 50  0000 C CNN
F 2 "digikey-footprints:0805" V 3830 5200 50  0001 C CNN
F 3 "~" H 3900 5200 50  0001 C CNN
F 4 "‎A129233CT-ND‎, P330ADCT-ND‎" V 3900 5200 50  0001 C CNN "Digi-Key Part Number"
	1    3900 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5BB9EE12
P 3550 3400
F 0 "R3" H 3620 3446 50  0000 L CNN
F 1 "10k" H 3620 3355 50  0000 L CNN
F 2 "digikey-footprints:0805" V 3480 3400 50  0001 C CNN
F 3 "~" H 3550 3400 50  0001 C CNN
F 4 "541-10KTACT-ND" H 3550 3400 50  0001 C CNN "Digi-Key Part Number"
	1    3550 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BB9EEA2
P 3500 5550
F 0 "R2" H 3570 5596 50  0000 L CNN
F 1 "10k" H 3570 5505 50  0000 L CNN
F 2 "digikey-footprints:0805" V 3430 5550 50  0001 C CNN
F 3 "~" H 3500 5550 50  0001 C CNN
F 4 "541-10KTACT-ND" H 3500 5550 50  0001 C CNN "Digi-Key Part Number"
	1    3500 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5400 3500 5200
Wire Wire Line
	4500 3300 4500 3700
Wire Wire Line
	3500 5700 3500 5900
Wire Wire Line
	3500 5900 4050 5900
Wire Wire Line
	4500 5900 4500 5400
$Comp
L power:GND #PWR02
U 1 1 5BBA0577
P 4050 6050
F 0 "#PWR02" H 4050 5800 50  0001 C CNN
F 1 "GND" H 4055 5877 50  0000 C CNN
F 2 "" H 4050 6050 50  0001 C CNN
F 3 "" H 4050 6050 50  0001 C CNN
	1    4050 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BBA05C6
P 4050 3900
F 0 "#PWR01" H 4050 3650 50  0001 C CNN
F 1 "GND" H 4055 3727 50  0000 C CNN
F 2 "" H 4050 3900 50  0001 C CNN
F 3 "" H 4050 3900 50  0001 C CNN
	1    4050 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3900 4050 3700
Wire Wire Line
	4050 6050 4050 5900
Connection ~ 4050 5900
Wire Wire Line
	4050 5900 4500 5900
Connection ~ 4050 3700
Wire Wire Line
	4050 3700 4500 3700
Wire Wire Line
	2250 5500 1750 5500
Wire Wire Line
	1750 5500 1750 5650
Wire Wire Line
	2250 5500 2250 5650
Wire Wire Line
	1750 5500 1150 5500
Wire Wire Line
	1150 5500 1150 5650
Connection ~ 1750 5500
Wire Wire Line
	2050 5650 2050 5400
Wire Wire Line
	2050 5400 1550 5400
Wire Wire Line
	950  5400 950  5650
Wire Wire Line
	1550 5650 1550 5400
Connection ~ 1550 5400
Wire Wire Line
	1550 5400 950  5400
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J3
U 1 1 5BBB2B4E
P 1750 1600
F 0 "J3" V 2250 1600 50  0000 C CNN
F 1 "RPi GPIO Ribbon Cable" V 2150 1600 50  0000 C CNN
F 2 "libraries:CNCTech-SMT-40pin_lock" H 1750 1600 50  0001 C CNN
F 3 "~" H 1750 1600 50  0001 C CNN
F 4 "1175-1763-ND" V 1750 1600 50  0001 C CNN "Digi-Key Part Number"
	1    1750 1600
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR011
U 1 1 5BBBF57C
P 9500 4250
F 0 "#PWR011" H 9500 4100 50  0001 C CNN
F 1 "+5V" H 9515 4423 50  0000 C CNN
F 2 "" H 9500 4250 50  0001 C CNN
F 3 "" H 9500 4250 50  0001 C CNN
	1    9500 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5BBC3674
P 10200 6050
F 0 "#PWR012" H 10200 5800 50  0001 C CNN
F 1 "GND" H 10205 5877 50  0000 C CNN
F 2 "" H 10200 6050 50  0001 C CNN
F 3 "" H 10200 6050 50  0001 C CNN
	1    10200 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 5550 10200 5550
Wire Wire Line
	10200 5650 10100 5650
Wire Wire Line
	4500 2550 4500 2700
Wire Wire Line
	4550 2550 4500 2550
Wire Wire Line
	4550 2450 4500 2450
Wire Wire Line
	4500 2450 4500 2550
Connection ~ 4500 2550
$Comp
L power:+5V #PWR03
U 1 1 5BBE4B42
P 4500 1950
F 0 "#PWR03" H 4500 1800 50  0001 C CNN
F 1 "+5V" H 4515 2123 50  0000 C CNN
F 2 "" H 4500 1950 50  0001 C CNN
F 3 "" H 4500 1950 50  0001 C CNN
	1    4500 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1950 4500 2100
Wire Wire Line
	4500 2350 4550 2350
Wire Wire Line
	4550 2250 4500 2250
Connection ~ 4500 2250
Wire Wire Line
	4500 2250 4500 2350
Wire Wire Line
	4550 4750 4500 4750
Wire Wire Line
	4500 4750 4500 4850
Wire Wire Line
	4550 4650 4500 4650
Wire Wire Line
	4500 4650 4500 4750
Connection ~ 4500 4750
$Comp
L power:+5V #PWR04
U 1 1 5BBF2C55
P 4500 4100
F 0 "#PWR04" H 4500 3950 50  0001 C CNN
F 1 "+5V" H 4515 4273 50  0000 C CNN
F 2 "" H 4500 4100 50  0001 C CNN
F 3 "" H 4500 4100 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4100 4500 4300
Wire Wire Line
	4500 4550 4550 4550
Wire Wire Line
	4550 4450 4500 4450
Connection ~ 4500 4450
Wire Wire Line
	4500 4450 4500 4550
Wire Wire Line
	3550 3700 4050 3700
Wire Wire Line
	3550 3100 3750 3100
Wire Wire Line
	3550 3100 3550 3250
Wire Wire Line
	3550 3700 3550 3550
Wire Wire Line
	4050 3100 4200 3100
Wire Wire Line
	4050 5200 4200 5200
Wire Wire Line
	2150 5650 2150 5250
$Comp
L Device:R R1
U 1 1 5BC21C95
P 1000 4600
F 0 "R1" V 1100 4550 50  0000 L CNN
F 1 "4.7k" V 1200 4500 50  0000 L CNN
F 2 "digikey-footprints:0805" V 930 4600 50  0001 C CNN
F 3 "~" H 1000 4600 50  0001 C CNN
F 4 "A129546CT-ND" V 1000 4600 50  0001 C CNN "Digi-Key Part Number"
	1    1000 4600
	0    1    1    0   
$EndComp
$Comp
L pspice:DIODE D2
U 1 1 5BC34500
P 4150 2400
F 0 "D2" V 4196 2272 50  0000 R CNN
F 1 "DIODE" V 4105 2272 50  0000 R CNN
F 2 "Diode_SMD:D_MiniMELF" H 4150 2400 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/FD/FDLL4148.pdf" H 4150 2400 50  0001 C CNN
F 4 "FDLL4148CT-ND" V 4150 2400 50  0001 C CNN "Digi-Key Part Number"
	1    4150 2400
	0    -1   -1   0   
$EndComp
$Comp
L pspice:DIODE D1
U 1 1 5BC347D9
P 4100 4600
F 0 "D1" V 4146 4472 50  0000 R CNN
F 1 "DIODE" V 4055 4472 50  0000 R CNN
F 2 "Diode_SMD:D_MiniMELF" H 4100 4600 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/FD/FDLL4148.pdf" H 4100 4600 50  0001 C CNN
F 4 "FDLL4148CT-ND" V 4100 4600 50  0001 C CNN "Digi-Key Part Number"
	1    4100 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 4400 4100 4300
Wire Wire Line
	4100 4300 4500 4300
Connection ~ 4500 4300
Wire Wire Line
	4500 4300 4500 4450
Wire Wire Line
	4100 4800 4100 4850
Wire Wire Line
	4100 4850 4500 4850
Connection ~ 4500 4850
Wire Wire Line
	4500 4850 4500 5000
Wire Wire Line
	4150 2200 4150 2100
Wire Wire Line
	4150 2100 4500 2100
Connection ~ 4500 2100
Wire Wire Line
	4500 2100 4500 2250
Wire Wire Line
	4150 2600 4150 2700
Wire Wire Line
	4150 2700 4500 2700
Connection ~ 4500 2700
Wire Wire Line
	4500 2700 4500 2900
Wire Wire Line
	10200 5650 10200 5900
Text Notes 1100 6350 0    50   ~ 0
1-wire temp. sensors.\nData line is pulled up to 3.3V.
Text Notes 3250 1750 0    50   ~ 0
The heater element relay switching circuits.\nEach MOSFET will switch 2 relays, each\nrelay draws about 25mA.
Text Notes 8500 3900 0    50   ~ 0
The RPi touchscreen only requires power.\nWith the RPi 3B+, the I2C connections are\nhandled in the ribbon cable.
$Comp
L Device:Q_NMOS_GSD Q2
U 1 1 5BCFE98A
P 4400 5200
F 0 "Q2" H 4605 5246 50  0000 L CNN
F 1 "RE1C002UN" H 4605 5155 50  0000 L CNN
F 2 "libraries:SOT-416FL" H 4600 5300 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RE1C002UN/re1c002untcl-e" H 4400 5200 50  0001 C CNN
F 4 "RE1C002UNTCLCT-ND" H 4400 5200 50  0001 C CNN "Digi-Key Part Number"
	1    4400 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q1
U 1 1 5BD0261C
P 4400 3100
F 0 "Q1" H 4605 3146 50  0000 L CNN
F 1 "RE1C002UN" H 4605 3055 50  0000 L CNN
F 2 "libraries:SOT-416FL" H 4600 3200 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RE1C002UN/re1c002untcl-e" H 4400 3100 50  0001 C CNN
F 4 "RE1C002UNTCLCT-ND" H 4400 3100 50  0001 C CNN "Digi-Key Part Number"
	1    4400 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5C1938D1
P 6450 3050
F 0 "R8" V 6243 3050 50  0000 C CNN
F 1 "0-330" V 6334 3050 50  0000 C CNN
F 2 "digikey-footprints:0805" V 6380 3050 50  0001 C CNN
F 3 "~" H 6450 3050 50  0001 C CNN
F 4 "‎A129233CT-ND‎, P330ADCT-ND‎" V 6450 3050 50  0001 C CNN "Digi-Key Part Number"
	1    6450 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C1938D8
P 6050 3400
F 0 "R6" H 6120 3446 50  0000 L CNN
F 1 "10k" H 6120 3355 50  0000 L CNN
F 2 "digikey-footprints:0805" V 5980 3400 50  0001 C CNN
F 3 "~" H 6050 3400 50  0001 C CNN
F 4 "541-10KTACT-ND" H 6050 3400 50  0001 C CNN "Digi-Key Part Number"
	1    6050 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3250 6050 3050
Wire Wire Line
	6050 3550 6050 3750
Wire Wire Line
	6050 3750 6600 3750
Wire Wire Line
	7050 3750 7050 3250
$Comp
L power:GND #PWR05
U 1 1 5C1938E3
P 6600 3900
F 0 "#PWR05" H 6600 3650 50  0001 C CNN
F 1 "GND" H 6605 3727 50  0000 C CNN
F 2 "" H 6600 3900 50  0001 C CNN
F 3 "" H 6600 3900 50  0001 C CNN
	1    6600 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3900 6600 3750
Connection ~ 6600 3750
Wire Wire Line
	6600 3750 7050 3750
Wire Wire Line
	7050 1950 7050 2150
Wire Wire Line
	6600 3050 6750 3050
$Comp
L pspice:DIODE D3
U 1 1 5C193904
P 6650 2450
F 0 "D3" V 6696 2322 50  0000 R CNN
F 1 "DIODE" V 6605 2322 50  0000 R CNN
F 2 "Diode_SMD:D_MiniMELF" H 6650 2450 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/FD/FDLL4148.pdf" H 6650 2450 50  0001 C CNN
F 4 "FDLL4148CT-ND" V 6650 2450 50  0001 C CNN "Digi-Key Part Number"
	1    6650 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 2250 6650 2150
Wire Wire Line
	6650 2150 7050 2150
Connection ~ 7050 2150
Wire Wire Line
	6650 2650 6650 2700
Wire Wire Line
	6650 2700 7050 2700
Connection ~ 7050 2700
Wire Wire Line
	7050 2700 7050 2850
Wire Wire Line
	6050 3050 6300 3050
$Comp
L Device:Q_NMOS_GSD Q3
U 1 1 5C193914
P 6950 3050
F 0 "Q3" H 7155 3096 50  0000 L CNN
F 1 "RE1C002UN" H 7155 3005 50  0000 L CNN
F 2 "libraries:SOT-416FL" H 7150 3150 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RE1C002UN/re1c002untcl-e" H 6950 3050 50  0001 C CNN
F 4 "RE1C002UNTCLCT-ND" H 6950 3050 50  0001 C CNN "Digi-Key Part Number"
	1    6950 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C1B0AB2
P 6450 5200
F 0 "R9" V 6243 5200 50  0000 C CNN
F 1 "0-330" V 6334 5200 50  0000 C CNN
F 2 "digikey-footprints:0805" V 6380 5200 50  0001 C CNN
F 3 "~" H 6450 5200 50  0001 C CNN
F 4 "‎A129233CT-ND‎, P330ADCT-ND‎" V 6450 5200 50  0001 C CNN "Digi-Key Part Number"
	1    6450 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5C1B0AB9
P 6050 5550
F 0 "R7" H 6120 5596 50  0000 L CNN
F 1 "10k" H 6120 5505 50  0000 L CNN
F 2 "digikey-footprints:0805" V 5980 5550 50  0001 C CNN
F 3 "~" H 6050 5550 50  0001 C CNN
F 4 "541-10KTACT-ND" H 6050 5550 50  0001 C CNN "Digi-Key Part Number"
	1    6050 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5400 6050 5200
Wire Wire Line
	6050 5700 6050 5900
Wire Wire Line
	6050 5900 6600 5900
Wire Wire Line
	7050 5900 7050 5400
$Comp
L power:GND #PWR06
U 1 1 5C1B0AC4
P 6600 6050
F 0 "#PWR06" H 6600 5800 50  0001 C CNN
F 1 "GND" H 6605 5877 50  0000 C CNN
F 2 "" H 6600 6050 50  0001 C CNN
F 3 "" H 6600 6050 50  0001 C CNN
	1    6600 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 6050 6600 5900
Connection ~ 6600 5900
Wire Wire Line
	6600 5900 7050 5900
$Comp
L power:+5V #PWR08
U 1 1 5C1B0AD2
P 7050 4100
F 0 "#PWR08" H 7050 3950 50  0001 C CNN
F 1 "+5V" H 7065 4273 50  0000 C CNN
F 2 "" H 7050 4100 50  0001 C CNN
F 3 "" H 7050 4100 50  0001 C CNN
	1    7050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4100 7050 4300
Wire Wire Line
	6600 5200 6750 5200
$Comp
L pspice:DIODE D4
U 1 1 5C1B0ADE
P 6650 4600
F 0 "D4" V 6696 4472 50  0000 R CNN
F 1 "DIODE" V 6605 4472 50  0000 R CNN
F 2 "Diode_SMD:D_MiniMELF" H 6650 4600 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/FD/FDLL4148.pdf" H 6650 4600 50  0001 C CNN
F 4 "FDLL4148CT-ND" V 6650 4600 50  0001 C CNN "Digi-Key Part Number"
	1    6650 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 4400 6650 4300
Wire Wire Line
	6650 4300 7050 4300
Connection ~ 7050 4300
Wire Wire Line
	6650 4800 6650 4850
Wire Wire Line
	6650 4850 7050 4850
Connection ~ 7050 4850
Wire Wire Line
	7050 4850 7050 5000
Wire Wire Line
	6050 5200 6300 5200
$Comp
L Device:Q_NMOS_GSD Q4
U 1 1 5C1B0AEE
P 6950 5200
F 0 "Q4" H 7155 5246 50  0000 L CNN
F 1 "RE1C002UN" H 7155 5155 50  0000 L CNN
F 2 "libraries:SOT-416FL" H 7150 5300 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RE1C002UN/re1c002untcl-e" H 6950 5200 50  0001 C CNN
F 4 "RE1C002UNTCLCT-ND" H 6950 5200 50  0001 C CNN "Digi-Key Part Number"
	1    6950 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 6600 5450 3050
Wire Wire Line
	5450 3050 6050 3050
Connection ~ 6050 3050
Wire Wire Line
	6050 5200 5650 5200
Wire Wire Line
	5650 5200 5650 6750
Connection ~ 6050 5200
Wire Notes Line
	5800 6350 5800 1400
Text Notes 6200 1650 0    50   ~ 0
The pump relay switching circuits.\nEach MOSFET will switch one relay.
Wire Notes Line
	5350 1400 5350 6300
Wire Notes Line
	3050 1400 3050 6300
Wire Wire Line
	1450 1800 1450 3100
Wire Wire Line
	7050 4300 7050 4550
Wire Wire Line
	7050 4650 7050 4850
Wire Wire Line
	7050 2150 7050 2400
Wire Wire Line
	7050 2500 7050 2700
Wire Notes Line
	8200 1400 8200 6350
Wire Notes Line
	3050 6300 5350 6300
Wire Notes Line
	3050 1400 5350 1400
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5C34B62F
P 7650 3800
F 0 "J7" H 7730 3792 50  0000 L CNN
F 1 "SSR Pumps" H 7730 3701 50  0000 L CNN
F 2 "libraries:TEConnectivity_4pos_90deg" H 7650 3800 50  0001 C CNN
F 3 "~" H 7650 3800 50  0001 C CNN
F 4 "A108794CT-ND" H 7650 3800 50  0001 C CNN "Digi-Key Part Number"
	1    7650 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2500 7300 3800
Wire Wire Line
	7300 3800 7450 3800
Wire Wire Line
	7050 2500 7300 2500
Wire Wire Line
	7050 2400 7450 2400
Wire Wire Line
	7450 2400 7450 3700
Wire Wire Line
	7450 3900 7300 3900
Wire Wire Line
	7300 3900 7300 4550
Wire Wire Line
	7050 4550 7300 4550
Wire Wire Line
	7450 4000 7450 4650
Wire Wire Line
	7050 4650 7450 4650
Text Notes 7550 3550 0    50   ~ 0
Pump 1
Text Notes 7550 4200 0    50   ~ 0
Pump 2
Wire Notes Line
	5800 6350 8200 6350
Wire Notes Line
	5800 1400 8200 1400
$Comp
L power:+5V #PWR07
U 1 1 5C1938F8
P 7050 1950
F 0 "#PWR07" H 7050 1800 50  0001 C CNN
F 1 "+5V" H 7065 2123 50  0000 C CNN
F 2 "" H 7050 1950 50  0001 C CNN
F 3 "" H 7050 1950 50  0001 C CNN
	1    7050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5200 3500 5200
Wire Wire Line
	2150 5250 1650 5250
Connection ~ 1650 5250
Wire Wire Line
	1650 5250 1650 5650
Wire Wire Line
	1050 5250 1650 5250
Wire Wire Line
	1050 5250 1050 5650
Connection ~ 1150 5500
Wire Wire Line
	1150 1800 1150 4600
Wire Wire Line
	850  1800 850  4600
Wire Wire Line
	850  5250 1050 5250
Connection ~ 1050 5250
Connection ~ 1150 4600
Wire Wire Line
	1150 4600 1150 5500
Connection ~ 850  4600
Wire Wire Line
	850  4600 850  5250
Wire Notes Line
	700  6450 2450 6450
Wire Notes Line
	2450 6450 2450 4250
Wire Notes Line
	2450 4250 700  4250
Wire Notes Line
	700  4250 700  6450
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 5C240092
P 9400 3150
F 0 "J9" H 9550 3050 50  0000 C CNN
F 1 "120VAC Pin" H 9450 2950 50  0000 C CNN
F 2 "libraries:Wurth_TermBlk_2Pos" H 9400 3150 50  0001 C CNN
F 3 "~" H 9400 3150 50  0001 C CNN
F 4 "1297-1108-1-ND‎" V 9400 3150 50  0001 C CNN "Digi-Key Part Number"
	1    9400 3150
	1    0    0    -1  
$EndComp
$Comp
L Recom:RAC20-K PS1
U 1 1 5C25089A
P 9300 2250
F 0 "PS1" V 9550 2600 50  0000 C CNN
F 1 "RAC20-K" V 9450 2650 50  0000 C CNN
F 2 "libraries:Recom_RAC20-05SK" H 9300 2500 50  0001 C CNN
F 3 "" H 9300 2500 50  0001 C CNN
F 4 "‎945-3190-ND‎" V 9300 2250 50  0001 C CNN "Digi-Key Part Number"
	1    9300 2250
	0    -1   -1   0   
$EndComp
Text Notes 8850 1100 0    50   ~ 0
120VAC - 5VDC Recom PS
Wire Wire Line
	10200 5550 10200 5650
Connection ~ 10200 5650
Wire Wire Line
	9500 4250 9500 4750
$Comp
L Connector_Generic:Conn_01x05 J8
U 1 1 5BB99B1D
P 8650 4950
F 0 "J8" H 8750 5300 50  0000 R CNN
F 1 "7\" Touchscreen Connector" H 9050 4650 50  0000 R CNN
F 2 "libraries:TEConnectivity_5pos_90deg" H 8650 4950 50  0001 C CNN
F 3 "~" H 8650 4950 50  0001 C CNN
F 4 "A100864CT-ND" H 8650 4950 50  0001 C CNN "Digi-Key Part Number"
	1    8650 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	10200 5900 9150 5900
Wire Wire Line
	9150 5900 9150 5150
Wire Wire Line
	9150 5150 8850 5150
Connection ~ 10200 5900
Wire Wire Line
	10200 5900 10200 6050
Wire Wire Line
	8850 4750 9500 4750
Connection ~ 9500 4750
Wire Wire Line
	9500 4750 9500 5250
Wire Wire Line
	9400 2850 9400 2750
$Comp
L power:+5V #PWR09
U 1 1 5C2BFEDB
P 9200 1350
F 0 "#PWR09" H 9200 1200 50  0001 C CNN
F 1 "+5V" H 9215 1523 50  0000 C CNN
F 2 "" H 9200 1350 50  0001 C CNN
F 3 "" H 9200 1350 50  0001 C CNN
	1    9200 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5C2C01BE
P 9300 1600
F 0 "#PWR010" H 9300 1350 50  0001 C CNN
F 1 "GND" H 9305 1427 50  0000 C CNN
F 2 "" H 9300 1600 50  0001 C CNN
F 3 "" H 9300 1600 50  0001 C CNN
	1    9300 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9300 1750 9300 1700
Wire Wire Line
	9200 1750 9200 1600
Wire Wire Line
	2550 1800 2550 6750
Wire Wire Line
	2650 1800 2650 6600
Wire Wire Line
	2550 6750 5650 6750
Wire Wire Line
	2650 6600 5450 6600
Wire Wire Line
	1550 4150 3150 4150
Wire Wire Line
	1550 1800 1550 4150
Wire Wire Line
	3550 3100 1450 3100
Connection ~ 3550 3100
Wire Wire Line
	750  1200 1050 1200
Wire Wire Line
	1050 1200 1050 1300
Wire Wire Line
	950  5400 750  5400
Connection ~ 950  5400
Wire Wire Line
	750  5400 750  1200
$Comp
L Connector:TestPoint TP1
U 1 1 5C378280
P 8900 1600
F 0 "TP1" V 8900 1800 50  0000 L CNN
F 1 "+5V" V 8800 1800 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm_LowProfile" H 9100 1600 50  0001 C CNN
F 3 "~" H 9100 1600 50  0001 C CNN
F 4 " 36-5021-ND‎" V 8900 1600 50  0001 C CNN "Digi-Key Part Number"
	1    8900 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 1600 9200 1600
Connection ~ 9200 1600
Wire Wire Line
	9200 1600 9200 1350
$Comp
L Connector:TestPoint TP2
U 1 1 5C37DFEB
P 8900 2850
F 0 "TP2" V 8900 3050 50  0000 L CNN
F 1 "Neutral" V 8800 2900 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm_LowProfile" H 9100 2850 50  0001 C CNN
F 3 "~" H 9100 2850 50  0001 C CNN
F 4 " 36-5021-ND‎" V 8900 2850 50  0001 C CNN "Digi-Key Part Number"
	1    8900 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 2850 9200 2850
Wire Wire Line
	9200 2750 9200 2850
Connection ~ 9200 2850
$Comp
L Connector:TestPoint TP3
U 1 1 5C38A072
P 9950 2850
F 0 "TP3" V 9950 3050 50  0000 L CNN
F 1 "Line" V 10050 3050 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm_LowProfile" H 10150 2850 50  0001 C CNN
F 3 "~" H 10150 2850 50  0001 C CNN
F 4 "‎36-5021-ND‎" V 9950 2850 50  0001 C CNN "Digi-Key Part Number"
	1    9950 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	9950 2850 9700 2850
$Comp
L Connector:TestPoint TP5
U 1 1 5C3901EE
P 9700 1700
F 0 "TP5" V 9700 1900 50  0000 L CNN
F 1 "GND" V 9800 1900 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm_LowProfile" H 9900 1700 50  0001 C CNN
F 3 "~" H 9900 1700 50  0001 C CNN
F 4 " 36-5021-ND‎" V 9700 1700 50  0001 C CNN "Digi-Key Part Number"
	1    9700 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 1700 9550 1700
Connection ~ 9300 1700
Wire Wire Line
	9300 1700 9300 1600
Wire Wire Line
	3150 4150 3150 5200
Wire Wire Line
	3150 5200 3500 5200
Connection ~ 3500 5200
$Comp
L Connector:TestPoint TP4
U 1 1 5C3DE4AE
P 9700 1400
F 0 "TP4" V 9700 1600 50  0000 L CNN
F 1 "GND" V 9800 1600 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm_LowProfile" H 9900 1400 50  0001 C CNN
F 3 "~" H 9900 1400 50  0001 C CNN
F 4 " 36-5021-ND‎" V 9700 1400 50  0001 C CNN "Digi-Key Part Number"
	1    9700 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 1400 9550 1400
Wire Wire Line
	9550 1400 9550 1700
Connection ~ 9550 1700
Wire Wire Line
	9550 1700 9300 1700
Wire Wire Line
	9200 2850 9200 3150
Wire Wire Line
	9200 3250 9200 3450
Wire Wire Line
	9200 3450 9700 3450
Wire Wire Line
	9700 3450 9700 2850
Connection ~ 9700 2850
Wire Wire Line
	9700 2850 9400 2850
$EndSCHEMATC
