#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
A simple unit conversion module. Units are specific to the brewing industry.

File: UnitConvertor.py
Author: Adam Unger
"""

import sys
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QApplication, QMainWindow, QLineEdit, QLabel, QHBoxLayout, QVBoxLayout, QGroupBox, QPushButton, QComboBox
from PyQt5.QtGui import QDoubleValidator, QIcon, QFont, QDesktopServices
from PyQt5.QtCore import QUrl

CELSIUS_UNITS = 1
FAHRENHEIT_UNITS = 2

class UnitConvertor(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.resize(700, 500)
        self.center()
        self.setWindowTitle('Unit Conversion Calculator')
        self.setWindowIcon(QIcon('./Icons/unitIcon.png'))

        # Create density units
        self.sg = None
        self.temp = 26.0
        self.hydroTemp = 26.0
        self.tempUnits = CELSIUS_UNITS
        self.hydroTempUnits = CELSIUS_UNITS

        # Create the validator for the numerical line edits.
        doubleValidator = QDoubleValidator()
        # Create bold font for the result labels
        boldFont = QFont()
        boldFont.setBold(True)

        # Create the layouts
        # main layouts
        mainLayout = QHBoxLayout()
        # density calc's
        densityGroupBox = QGroupBox("Density Units")
        densityLayout = QVBoxLayout()
        gravityLayout = QHBoxLayout()
        gravityLabel = QLabel("Specific Gravity (SG):")
        self.gravityLineEdit = QLineEdit()
        self.gravityLineEdit.setValidator(doubleValidator)
        self.gravityLineEdit.editingFinished.connect(self.gravityChanged)
        platoLayout = QHBoxLayout()
        platoLabel = QLabel(str(u"\N{DEGREE SIGN}") + "Plato (" + str(u"\N{DEGREE SIGN}") + "P):")
        self.platoLineEdit = QLineEdit()
        self.platoLineEdit.setValidator(doubleValidator)
        self.platoLineEdit.editingFinished.connect(self.platoChanged)
        brixLayout = QHBoxLayout()
        brixLabel = QLabel(str(u"\N{DEGREE SIGN}") + "Brix (" + str(u"\N{DEGREE SIGN}") + "Bx):")
        self.brixLineEdit = QLineEdit()
        self.brixLineEdit.setValidator(doubleValidator)
        self.brixLineEdit.editingFinished.connect(self.brixChanged)
        tempCorrectionLayout = QHBoxLayout()
        tempCorrectionLabel = QLabel("Wort Temperature:")
        self.tempCorrectionLineEdit = QLineEdit()
        self.tempCorrectionLineEdit.setValidator(doubleValidator)
        self.tempCorrectionLineEdit.editingFinished.connect(self.tempChanged)
        self.tempCorrectionLineEdit.setText(str(self.temp))
        self.tempCorrectionLineEdit.setToolTip("The temperature of the wort at the time of the reading")
        tempCorrectionUnitsComboBox = QComboBox()
        tempCorrectionUnitsComboBox.addItem(str(u"\N{DEGREE SIGN}" + "C"))
        tempCorrectionUnitsComboBox.addItem(str(u"\N{DEGREE SIGN}" + "F"))
        tempCorrectionUnitsComboBox.currentIndexChanged.connect(self.tempCorrectionUnitsChanged)
        tempCorrectionDefaultButton = QPushButton("Default")
        tempCorrectionDefaultButton.setToolTip("Reset the temperature of the wort at the time of the hydrometer reading to 25" + str(u"\N{DEGREE SIGN}") + "C")
        tempCorrectionDefaultButton.clicked.connect(self.tempCorrectionSetDefaults)
        hydroCalLayout = QHBoxLayout()
        hydroCalLabel = QLabel("Calibrated Temperature:")
        self.hydroCalLineEdit = QLineEdit()
        self.hydroCalLineEdit.setValidator(doubleValidator)
        self.hydroCalLineEdit.editingFinished.connect(self.hydrometerCalibrationChanged)
        self.hydroCalLineEdit.setText(str(self.hydroTemp))
        self.hydroCalLineEdit.setToolTip("The calibrated temperature of the hydrometer, typically 15" + str(u"\N{DEGREE SIGN}") + "C")
        hydroCalUnitsComboBox = QComboBox()
        hydroCalUnitsComboBox.addItem(str(u"\N{DEGREE SIGN}" + "C"))
        hydroCalUnitsComboBox.addItem(str(u"\N{DEGREE SIGN}" + "F"))
        hydroCalUnitsComboBox.currentIndexChanged.connect(self.hydrometerCalibrationUnitsChanged)
        hydroCalDefaultButton = QPushButton("Default")
        hydroCalDefaultButton.setToolTip("Reset the calibrated temperature of the hydrometer to 15" + str(u"\N{DEGREE SIGN}") + "C")
        hydroCalDefaultButton.clicked.connect(self.hydrometerCalibrationSetDefaults)
        densityDescriptionLabel = QLabel('Density calculations are referenced to 20' + str(u"\N{DEGREE SIGN}") + 'C. If the reading was obtained\
                                        <br>at any other temperature, use the conversion button, above. The way that\
                                        <br>a refractometer determines Brix skews the reading when alcohol is present.\
                                        <br>This means that a refractometer is perfectly suitable for taking a reading\
                                        <br>to determine the Original Gravity (OG), but using one during and after\
                                        <br>fermentation can be inaccurate. When in doubt, best practice is to use a\
                                        <br>hydrometer any time after fermentation begins. The temperature\
                                        <br>compensation was achieved using information from these sites:\
                                        <br><a href="https://homebrew.stackexchange.com/questions/4137/temperature-correction-for-specific-gravity">StackExchange</a> and <a href="http://brewery.org/library/HydromCorr0992.html">brewery.org</a>')
        densityDescriptionLabel.setOpenExternalLinks(True)
        # mass/weight calc's
        massWeightGroupBox = QGroupBox("Mass and Weight Units")
        massWeightLayout = QVBoxLayout()
        kilogramLayout = QHBoxLayout()
        kilogramLabel = QLabel("Kilogram (kg):")
        self.kilogramLineEdit = QLineEdit()
        self.kilogramLineEdit.setValidator(doubleValidator)
        self.kilogramLineEdit.editingFinished.connect(self.kilogramChanged)
        poundLayout = QHBoxLayout()
        poundLabel = QLabel("Pound (lbs):")
        self.poundLineEdit = QLineEdit()
        self.poundLineEdit.setValidator(doubleValidator)
        self.poundLineEdit.editingFinished.connect(self.poundChanged)
        ounceLayout = QHBoxLayout()
        ounceLabel = QLabel("Ounce (oz):")
        self.ounceLineEdit = QLineEdit()
        self.ounceLineEdit.setValidator(doubleValidator)
        self.ounceLineEdit.editingFinished.connect(self.ounceChanged)
        # volumen calc's
        volumeGroupBox = QGroupBox("Volume Units")
        # pressure calc's
        pressureGroupBox = QGroupBox("Pressure Units")

        # Add all the layouts to each other
        # density calc's
        gravityLayout.addWidget(gravityLabel)
        gravityLayout.addWidget(self.gravityLineEdit)
        platoLayout.addWidget(platoLabel)
        platoLayout.addWidget(self.platoLineEdit)
        brixLayout.addWidget(brixLabel)
        brixLayout.addWidget(self.brixLineEdit)
        tempCorrectionLayout.addWidget(tempCorrectionLabel)
        tempCorrectionLayout.addWidget(self.tempCorrectionLineEdit)
        tempCorrectionLayout.addWidget(tempCorrectionUnitsComboBox)
        tempCorrectionLayout.addWidget(tempCorrectionDefaultButton)
        hydroCalLayout.addWidget(hydroCalLabel)
        hydroCalLayout.addWidget(self.hydroCalLineEdit)
        hydroCalLayout.addWidget(hydroCalUnitsComboBox)
        hydroCalLayout.addWidget(hydroCalDefaultButton)
        densityLayout.addLayout(gravityLayout)
        densityLayout.addLayout(platoLayout)
        densityLayout.addLayout(brixLayout)
        densityLayout.addLayout(tempCorrectionLayout)
        densityLayout.addLayout(hydroCalLayout)
        densityLayout.addWidget(densityDescriptionLabel)
        densityGroupBox.setLayout(densityLayout)
        # mass/weight calc's
        kilogramLayout.addWidget(kilogramLabel)
        kilogramLayout.addWidget(self.kilogramLineEdit)
        poundLayout.addWidget(poundLabel)
        poundLayout.addWidget(self.poundLineEdit)
        ounceLayout.addWidget(ounceLabel)
        ounceLayout.addWidget(self.ounceLineEdit)
        massWeightLayout.addLayout(kilogramLayout)
        massWeightLayout.addLayout(poundLayout)
        massWeightLayout.addLayout(ounceLayout)
        massWeightGroupBox.setLayout(massWeightLayout)

        # main layout's
        mainLayout.addWidget(densityGroupBox)
        mainLayout.addWidget(massWeightGroupBox)
        mainLayout.addWidget(volumeGroupBox)
        mainLayout.addWidget(pressureGroupBox)
        centralWidget = QWidget(self)
        centralWidget.setLayout(mainLayout)
        self.setCentralWidget(centralWidget)

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def gravityToPlato(self, sg):
        return (1111.14 * sg) - (630.272 * pow(sg, 2)) + (135.997 * pow(sg, 3)) - 616.868

    def gravityToBrix(self, sg):
        return (220.0 * (sg - 1.0)) + 1.6

    def platoToGravity(self, p):
        return 1.0 + (p / (258.6 - ((p / 258.2) * 227.1)))

    def platoToBrix(self, p):
        return self.gravityToBrix(self.platoToGravity(p))

    def brixToGravity(self, b):
        return ((b - 1.6) / 220.0) + 1.0

    def brixToPlato(self, b):
        return self.gravityToPlato(self.brixToGravity(b))

    def celsiusToFahrenheit(self, c):
        return c * 1.8 + 32.0

    def fahrenheitToCelsius(self, f):
        return (f - 32) / 1.8

    def psiToBar(self, p):
        return p * 0.0689476

    def psiTokPa(self, p):
        return p * 6.89476

    def barTokPa(self, b):
        return b * 100.0

    def barToPsi(self, b):
        return b * 14.503773773

    def kPaToPsi(self, p):
        return p * 0.145038

    def kPaToBar(self, p):
        return p * 0.01

    def kgToLbs(self, kg):
        return kg * 2.20462

    def kgToOz(self, kg):
        return kg * 35.274

    def lbsToKg(self, lbs):
        return lbs * 0.453592

    def lbsToOz(self, lbs):
        return lbs * 16.0

    def ozToKg(self, oz):
        return oz * 0.0283495

    def ozToLbs(self, oz):
        return oz * 0.0625

    def kilogramChanged(self):
        try:
            kg = float(self.kilogramLineEdit.text())
        except ValueError:
            kg = None
        else:
            if kg is not None:
                self.poundLineEdit.setText(str(round(self.kgToLbs(kg), 4)))
                self.ounceLineEdit.setText(str(round(self.kgToOz(kg), 4)))

    def poundChanged(self):
        try:
            lbs = float(self.poundLineEdit.text())
        except ValueError:
            lbs = None
        else:
            if lbs is not None:
                self.kilogramLineEdit.setText(str(round(self.lbsToKg(lbs), 4)))
                self.ounceLineEdit.setText(str(round(self.lbsToOz(lbs), 4)))

    def ounceChanged(self):
        try:
            oz = float(self.ounceLineEdit.text())
        except ValueError:
            oz = None
        else:
            if oz is not None:
                self.poundLineEdit.setText(str(round(self.ozToLbs(oz), 4)))
                self.kilogramLineEdit.setText(str(round(self.ozToKg(oz), 4)))

    def gravityChanged(self):
        try:
            self.sg = float(self.gravityLineEdit.text())
        except ValueError:
            self.sg = None
        else:
            self.calculateDensities()

    def platoChanged(self):
        try:
            self.sg = self.platoToGravity(float(self.platoLineEdit.text()))
        except ValueError:
            self.sg = None
        else:
            self.calculateDensities()

    def brixChanged(self):
        try:
            self.sg = self.brixToGravity(float(self.brixLineEdit.text()))
        except ValueError:
            self.sg = None
        else:
            self.calculateDensities()

    def tempChanged(self):
        '''This will calculate an offset for the densities to compensate for temperature.'''
        # start by getting what the user entered, if it's nothing, then do nothing
        try:
            temp = float(self.tempCorrectionLineEdit.text())
        except ValueError:
            self.temp = None
        else:
            if self.tempUnits == FAHRENHEIT_UNITS:
                self.temp = self.fahrenheitToCelsius(temp)
            else:
                self.temp = temp
            self.calculateDensities()

    def tempCorrectionUnitsChanged(self):
        '''Update the units that are displayed to the user. Internally, we always use celsius.'''
        if self.tempUnits == CELSIUS_UNITS:
            if self.temp is not None:
                self.tempUnits = FAHRENHEIT_UNITS
                self.tempCorrectionLineEdit.setText(str(round(self.celsiusToFahrenheit(self.temp), 1)))
        else:
            if self.temp is not None:
                self.tempUnits = CELSIUS_UNITS
                self.tempCorrectionLineEdit.setText(str(round(self.temp, 1)))

    def tempCorrectionSetDefaults(self):
        self.tempCorrectionLineEdit.setText('26.0')
        self.calculateDensities()

    def hydrometerCalibrationChanged(self):
        # start by getting what the user entered, if it's nothing, then do nothing
        try:
            temp = float(self.hydroCalLineEdit.text())
        except ValueError:
            self.hydroTemp = None
        else:
            if self.hydroTempUnits == FAHRENHEIT_UNITS:
                self.hydroTemp = self.fahrenheitToCelsius(temp)
            else:
                self.hydroTemp = temp
            self.calculateDensities()

    def hydrometerCalibrationUnitsChanged(self):
        '''Update the units that are displayed to the user. Internally, we always use celsius.'''
        if self.hydroTempUnits == CELSIUS_UNITS:
            if self.hydroTemp is not None:
                self.hydroTempUnits = FAHRENHEIT_UNITS
                self.hydroCalLineEdit.setText(str(round(self.celsiusToFahrenheit(self.hydroTemp), 1)))
        else:
            if self.hydroTemp is not None:
                self.hydroTempUnits = CELSIUS_UNITS
                self.hydroCalLineEdit.setText(str(round(self.hydroTemp, 1)))

    def hydrometerCalibrationSetDefaults(self):
        self.hydroCalLineEdit.setText('26.0')
        self.calculateDensities()

    def temperatureCorrectedGravity(self):
        return self.sg * ((1.00130346 - 0.000134722124 * self.celsiusToFahrenheit(self.temp) + 0.00000204052596 * pow(self.celsiusToFahrenheit(self.temp), 2) - 0.00000000232820948 * pow(self.celsiusToFahrenheit(self.temp), 3)) / (1.00130346 - 0.000134722124 * self.celsiusToFahrenheit(self.hydroTemp) + 0.00000204052596 * pow(self.celsiusToFahrenheit(self.hydroTemp), 2) - 0.00000000232820948 * pow(self.celsiusToFahrenheit(self.hydroTemp), 3)))
        #return 1.313454 - (0.132674 * self.celsiusToFahrenheit(self.temp)) + (0.002057793 * pow(self.celsiusToFahrenheit(self.temp), 2)) - (0.000002627634 * pow(self.celsiusToFahrenheit(self.temp), 3))

    def calculateDensities(self):
        if self.sg is not None and self.temp is not None and self.hydroTemp is not None:
            sg = self.temperatureCorrectedGravity()
            self.gravityLineEdit.setText(str(round(sg, 3)))
            self.platoLineEdit.setText(str(round(self.gravityToPlato(sg), 1)))
            self.brixLineEdit.setText(str(round(self.gravityToBrix(sg), 1)))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    unitConvertor = UnitConvertor()
    sys.exit(app.exec_())
