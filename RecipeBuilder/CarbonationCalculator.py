#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Calculates regulator PSI setting based on desired volumes of CO2 and temperature.

File: CarbonationCalculator.py
Author: Adam Unger
"""

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QApplication, QApplication, QMainWindow, QLineEdit, QLabel, QGridLayout, QRadioButton, QHBoxLayout
from PyQt5.QtGui import QDoubleValidator, QIcon

from UnitConvertor import UnitConvertor

class CarbonationCalculator(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        uic.loadUi('CarbonationCalculator.ui', self)

        # Init the constants
        self.HENRYS_LAW_WATER_CONSTANT = 5.16
        self.DENSITY_OF_ETHANOL = 0.789
        self.DEFAULT_SG = 1.015
        self.DEFAULT_BAROMETRIC_PRESSURE = 14.696

        # Init the variables
        self.volumes = None
        self.temp = None
        self.tempUnits = str(u"\N{DEGREE SIGN}") + "C"
        self.alcohol = None
        self.alcoholUnits = "% (v/v)"
        self.sg = 1.015
        self.baro = 14.696
        self.baroUnits = "psi"
        self.result = None
        self.resultUnits = "psi"

        self.unitConvertor = UnitConvertor()

        # Create the validator for the volumes and temperature line edits.
        doubleValidator = QDoubleValidator()

        # Setup the volumes of CO2 line edit
        self.volumesLineEdit.setValidator(doubleValidator)
        self.volumesLineEdit.textEdited.connect(self.setVolumes)
        # Setup the temperature line edit
        self.tempLineEdit.setValidator(doubleValidator)
        self.tempLineEdit.textEdited.connect(self.setTemp)
        # Setup the temperature units combo box
        self.tempComboBox.addItems([str(u"\N{DEGREE SIGN}") + "C", str(u"\N{DEGREE SIGN}") + "F"])
        self.tempComboBox.currentIndexChanged.connect(self.tempUnitsChanged)
        # Setup the alcohol line edit
        self.alcoholLineEdit.setValidator(doubleValidator)
        self.alcoholLineEdit.textEdited.connect(self.setAlcohol)
        # Setup the alcohol content combo box
        self.alcoholComboBox.currentIndexChanged.connect(self.alcoholUnitsChanged)
        # Setup the specific gravity line edit
        self.sgLineEdit.setValidator(doubleValidator)
        self.sgLineEdit.textEdited.connect(self.setSG)
        # Setup the default button for the specific gravity
        self.sgDefaultButton.clicked.connect(self.setDefaultSG)
        # Setup the barometric pressure line edit
        self.baroPressureLineEdit.setValidator(doubleValidator)
        self.baroPressureLineEdit.textEdited.connect(self.setBaro)
        # Setup the barometric pressure combo box
        self.baroPressureComboBox.currentIndexChanged.connect(self.baroPressureUnitsChanged)
        # Setup the barometric pressure default button
        self.baroPressureDefaultButton.clicked.connect(self.setDefaultBaroPressure)
        # Setup the results line edit
        self.resultLineEdit.setValidator(doubleValidator)
        self.resultLineEdit.textEdited.connect(self.setResult)
        # Setup the result units combo box
        self.resultComboBox.currentIndexChanged.connect(self.resultUnitsChanged)

        # Show the main window
        self.show()

    def setVolumes(self):
        try:
            self.volumes = float(self.volumesLineEdit.text())
        except ValueError:
            self.volumes = None

        self.calculatePressure()

    def setTemp(self):
        try:
            self.temp = float(self.tempLineEdit.text())
        except ValueError:
            self.temp = None

        self.calculatePressure()

    def setAlcohol(self):
        try:
            self.alcohol = float(self.alcoholLineEdit.text())
        except ValueError:
            self.alcohol = None

        self.calculatePressure()

    def setSG(self):
        try:
            self.sg = float(self.sgLineEdit.text())
        except ValueError:
            self.sg = None

        self.calculatePressure()

    def setBaro(self):
        try:
            self.baro = float(self.baroPressureLineEdit.text())
        except ValueError:
            self.baro = None

        self.calculatePressure()

    def setResult(self):
        try:
            self.result = float(self.resultLineEdit.text())
        except ValueError:
            self.result = None
        else:
            self.calculateVolumes()

    def volToWeight(self, v):
        return v * 0.8

    def weightToVol(self, w):
        return w * 1.25

    def setDefaultSG(self):
        self.sg = self.DEFAULT_SG
        self.sgLineEdit.setText(str(round(self.DEFAULT_SG, 3)))
        self.calculatePressure()

    def setDefaultBaroPressure(self):
        self.baro = self.DEFAULT_BAROMETRIC_PRESSURE
        self.baroUnits = "psi"
        self.baroPressureLineEdit.setText(str(round(self.DEFAULT_BAROMETRIC_PRESSURE, 3)))
        self.baroPressureComboBox.setCurrentIndex(0)
        self.calculatePressure()

    def tempUnitsChanged(self):
        ''' Convert the previous number into the new units and update the
            display. Since a units change won't affect the end result, we don't
            need to recalculate. '''
        # convert the text in the line edit to the appropriate units and update our internal variables
        if self.tempUnits == str(u"\N{DEGREE SIGN}") + "C":
            self.temp = self.unitConvertor.celsiusToFahrenheit(self.temp)
            self.tempUnits = str(u"\N{DEGREE SIGN}") + "F"
        else:
            self.temp = self.unitConvertor.fahrenheitToCelsius(self.temp)
            self.tempUnits = str(u"\N{DEGREE SIGN}") + "C"

        # now update what the user sees
        self.tempLineEdit.setText(str(round(self.temp, 2)))

    def alcoholUnitsChanged(self):
        ''' Convert the previous number into the new units and update the
            display. Since a units change won't affect the end result, we don't
            need to recalculate. '''
        if self.alcoholUnits == "% (v/v)":
            self.alcohol = self.unitConvertor.volToWeight(self.alcohol)
            self.alcoholUnits = "% (w/w)"
        else:
            self.alcohol = self.unitConvertor.weightToVol(self.alcohol)
            self.alcoholUnits = "% (v/v)"

        # now update what the user sees
        self.alcoholLineEdit.setText(str(round(self.alcohol, 2)))

    def baroPressureUnitsChanged(self):
        newUnits = self.baroPressureComboBox.currentText()
        if self.baroUnits == "kPa" and newUnits == "psi":
            self.baro = self.unitConvertor.kPaToPsi(self.baro)
            self.baroUnits = "psi"
        elif self.baroUnits == "kPa" and newUnits == "bar":
            self.baro = self.unitConvertor.kPaToBar(self.baro)
            self.baroUnits = "bar"
        elif self.baroUnits == "bar" and newUnits == "psi":
            self.baro = self.unitConvertor.barToPsi(self.baro)
            self.baroUnits = "psi"
        elif self.baroUnits == "bar" and newUnits == "kPa":
            self.baro = self.unitConvertor.barTokPa(self.baro)
            self.baroUnits = "kPa"
        elif self.baroUnits == "psi" and newUnits == "kPa":
            self.baro = self.unitConvertor.psiTokPa(self.baro)
            self.baroUnits = "kPa"
        elif self.baroUnits == "psi" and newUnits == "bar":
            self.baro = self.unitConvertor.psiToBar(self.baro)
            self.baroUnits = "bar"

        # now update what the user sees
        self.baroPressureLineEdit.setText(str(round(self.baro, 3)))

    def resultUnitsChanged(self):
        newUnits = self.resultComboBox.currentText()
        if self.resultUnits == "kPa" and newUnits == "psi":
            self.result = self.unitConvertor.kPaToPsi(self.result)
            self.resultUnits = "psi"
        elif self.resultUnits == "kPa" and newUnits == "bar":
            self.result = self.unitConvertor.kPaToBar(self.result)
            self.resultUnits = "bar"
        elif self.resultUnits == "bar" and newUnits == "psi":
            self.result = self.unitConvertor.barToPsi(self.result)
            self.resultUnits = "psi"
        elif self.resultUnits == "bar" and newUnits == "kPa":
            self.result = self.unitConvertor.barTokPa(self.result)
            self.resultUnits = "kPa"
        elif self.resultUnits == "psi" and newUnits == "kPa":
            self.result = self.unitConvertor.psiTokPa(self.result)
            self.resultUnits = "kPa"
        elif self.resultUnits == "psi" and newUnits == "bar":
            self.result = self.unitConvertor.psiToBar(self.result)
            self.resultUnits = "bar"

        # now update what the user sees
        self.resultLineEdit.setText(str(round(self.result, 3)))

    def calculateVolumes(self):
        ''' Calculate the volumes (volume of CO2 / volume of beer). The formula
            used comes from Vital Sensors Technologies -
            http://vitalsensorstech.com/PDF%27s/Methods%20of%20Analysis%20for%20correcting%20CO2%20content%20for%20Specific%20Gravity%20and%20Alcohol.pdf,
            equation 4. '''

        if self.result is not None and self.temp is not None and self.alcohol is not None and self.sg is not None and self.baro is not None:
            # Now ensure that the variables are in the units that the formula expects
            A = self.alcohol
            T = self.temp
            B = self.baro
            R = self.result
            if self.alcoholUnits == "% (v/v)":
                A = self.volToWeight(self.alcohol)
            if self.tempUnits == str(u"\N{DEGREE SIGN}") + "C":
                T = self.unitConvertor.celsiusToFahrenheit(self.temp)
            if self.baroUnits == "kPa":
                B = self.unitConvertor.kPaToPsi(self.baro)
            elif self.baroUnits == "bar":
                B = self.unitConvertor.barToPsi(self.baro)
            if self.resultUnits == "kPa":
                R = self.unitConvertor.kPaToPsi(self.result)
            elif self.resultUnits == "bar":
                R = self.unitConvertor.barToPsi(self.result)

            # Now calculate the volumes of the resultant mixture
            denominator = (T + 12.4) * (1 + (A / 100 / self.DENSITY_OF_ETHANOL)) * self.sg
            if denominator != 0:
                self.volumes = self.HENRYS_LAW_WATER_CONSTANT * (self.result + B) / denominator
                #self.volumes = (self.volumes * self.sg * (T + 12.4) * (1 + (A / 100 / self.DENSITY_OF_ETHANOL)) / self.HENRYS_LAW_WATER_CONSTANT) - B
                print(self.volumes, self.temp, self.alcohol, self.sg, self.baro, self.result)
                self.volumesLineEdit.setText(str(round(self.volumes, 2)))
            else:
                self.volumesLineEdit.setText('')
        else:
            self.volumesLineEdit.setText('')

    def calculatePressure(self):
        ''' Calculate the regulator pressure. The formula used comes from Vital
            Sensors Technologies -
            http://vitalsensorstech.com/PDF%27s/Methods%20of%20Analysis%20for%20correcting%20CO2%20content%20for%20Specific%20Gravity%20and%20Alcohol.pdf,
            equation 4. '''

        if self.volumes is not None and self.temp is not None and self.alcohol is not None and self.sg is not None and self.baro is not None:
            # Now ensure that the variables are in the units that the formula expects
            A = self.alcohol
            T = self.temp
            B = self.baro
            if self.alcoholUnits == "% (v/v)":
                A = self.volToWeight(self.alcohol)
            if self.tempUnits == str(u"\N{DEGREE SIGN}") + "C":
                T = self.unitConvertor.celsiusToFahrenheit(self.temp)
            if self.baroUnits == "kPa":
                B = self.unitConvertor.kPaToPsi(self.baro)
            elif self.baroUnits == "bar":
                B = self.unitConvertor.barToPsi(self.baro)

            # Now calculate the gauge pressure of the C02 tank
            self.result = (self.volumes * self.sg * (T + 12.4) * (1 + (A / 100 / self.DENSITY_OF_ETHANOL)) / self.HENRYS_LAW_WATER_CONSTANT) - B
            print(self.volumes, self.temp, self.alcohol, self.sg, self.baro, self.result)
            self.resultLineEdit.setText(str(round(self.result, 2)))
        else:
            self.resultLineEdit.setText('')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    carbonationCalculator = CarbonationCalculator()
    sys.exit(app.exec_())
