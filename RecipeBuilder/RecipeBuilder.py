#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Beer recipe creator.

This is the main file for the Beer recipe creator program.

File: RecipeBuilder.py
Author: Adam Unger
"""

import sys
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QApplication, QAction, qApp, QApplication, QMainWindow, QStyle
from PyQt5.QtGui import QIcon
from CarbonationCalculator import CarbonationCalculator
from BoilOffDilutionCalculator import BoilOffDilutionCalculator
from UnitConvertor import UnitConvertor


"""
This is the main class for the Brew Calculator.
"""
class RecipeCreatorMain(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.resize(500, 600)
        self.center()
        self.setWindowTitle('Recipe Builder')
        self.setWindowIcon(QIcon('./Icons/mainIcon.png'))
        self.createMenuBar()
        self.createCalculatorsToolbar()
        self.statusBar().showMessage('Keep calm and chive on!')
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def createMenuBar(self):
        # create the save recipe button
        saveAct = QAction(self.style().standardIcon(QStyle.SP_DialogSaveButton), 'Save', self)
        saveAct.setShortcut('Ctrl+S')
        saveAct.setStatusTip('Save changes to your recipe')
        saveAct.triggered.connect(self.saveRecipe)
        # create the exit app toolbar button
        exitAct = QAction(self.style().standardIcon(QStyle.SP_TitleBarCloseButton), 'Leave the app', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit')
        exitAct.triggered.connect(qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(saveAct)
        fileMenu.addAction(exitAct)

    def createCalculatorsToolbar(self):
        # create the unit converter button
        unitConverterAct = QAction(QIcon('./Icons/unitIcon.png'), 'Convert various units', self)
        #unitConverterAct.setShortcut('Ctrl+S')
        unitConverterAct.setStatusTip('Convert various units to other units')
        unitConverterAct.triggered.connect(self.unitConverterCalculator)

        # create the ABV calculator button
        #abvAct = QAction(self.style().standardIcon(QStyle.SP_DialogSaveButton), 'ABV Calculator', self)
        abvAct = QAction(QIcon('./Icons/abvIcon.png'), 'ABV calculator', self)
        #abvAct.setShortcut('Ctrl+S')
        abvAct.setStatusTip('Determine ABV based on initial and final SG (or Brix, or Plato, or ...)')
        abvAct.triggered.connect(self.abvCalculator)

        # create the hydrometer temperature compensation calculator button
        # This is being done by the unit conversion module....maybe delete this? Remove from unit conversion module?
        #hydrometerTempAct = QAction(QIcon('./Icons/hydrometerTemperatureIcon.png'), 'Hydrometer temperature compensation calculator', self)
        #hydrometerTempAct.setShortcut('Ctrl+S')
        #hydrometerTempAct.setStatusTip('Calculates a temperature compensated hydrometer reading')
        #hydrometerTempAct.triggered.connect(self.hydrometerTemperatureCalculator)

        # create the ABV calculator button
        ibuAct = QAction(QIcon('./Icons/ibuIcon.png'), 'IBU calculator', self)
        #ibuAct.setShortcut('Ctrl+S')
        ibuAct.setStatusTip('Calculates IBU based on hops additions and wort gravity')
        ibuAct.triggered.connect(self.ibuCalculator)

        # create the SRM calculator button
        srmAct = QAction(QIcon('./Icons/srmIcon.png'), 'SRM calculator', self)
        #srmAct.setShortcut('Ctrl+S')
        srmAct.setStatusTip('Determine how light or dark your beer will be based on the amount and type of ingredients')
        srmAct.triggered.connect(self.srmCalculator)

        # create the dilution and boil off calculator button
        dilutionBoilOffAct = QAction(QIcon('./Icons/dilutionBoilOffIcon.png'), 'Dilution and boil off calculator', self)
        #dilutionBoilOffAct.setShortcut('Ctrl+S')
        dilutionBoilOffAct.setStatusTip('Calculates how much you need to dilute or boil down your wort volume to hit a certain gravity')
        dilutionBoilOffAct.triggered.connect(self.dilutionBoilOffCalculator)

        # create the yeast pitch rate and starter calculator button
        yeastAct = QAction(QIcon('./Icons/yeastIcon.png'), 'Yeast pitch rate and starter calculator', self)
        #yeastAct.setShortcut('Ctrl+S')
        yeastAct.setStatusTip('Calculates yeast viability, starter size and how much DME to add to that starter')
        yeastAct.triggered.connect(self.yeastCalculator)

        # create the refractometer unit convertor calculator button
        refractometerAct = QAction(QIcon('./Icons/refractometerIcon.png'), 'Convert Brix WRI to various other units', self)
        #refractometerAct.setShortcut('Ctrl+S')
        refractometerAct.setStatusTip('Converts refractometer readings taken of wort (Brix WRI) to their actual value in Brix / Plato and Specific Gravity')
        refractometerAct.triggered.connect(self.refractometerCalculator)

        # create the carbonation calculator button
        carbonationAct = QAction(QIcon('./Icons/carbonationIcon.png'), 'Calculates regulator pressure setting', self)
        #carbonationAct.setShortcut('Ctrl+S')
        carbonationAct.setStatusTip('Calculates regulator pressure setting based on desired volumes of CO2 and temperature')
        carbonationAct.triggered.connect(self.carbonationCalculator)

        # create the carbonation calculator button
        waterAct = QAction(QIcon('./Icons/waterIcon.png'), 'Calculates water adjustments', self)
        #waterAct.setShortcut('Ctrl+S')
        waterAct.setStatusTip('Calculates water adjustments')
        waterAct.triggered.connect(self.waterCalculator)

        # now add a new toolbar and add the above actions
        self.toolbar = self.addToolBar('Calculators Toolbar')
        self.toolbar.addAction(unitConverterAct)
        self.toolbar.addAction(abvAct)
        #self.toolbar.addAction(hydrometerTempAct)
        self.toolbar.addAction(ibuAct)
        self.toolbar.addAction(srmAct)
        self.toolbar.addAction(dilutionBoilOffAct)
        self.toolbar.addAction(yeastAct)
        self.toolbar.addAction(refractometerAct)
        self.toolbar.addAction(carbonationAct)
        self.toolbar.addAction(waterAct)

    def waterCalculator(self):
        pass

    def carbonationCalculator(self):
        self.cc = CarbonationCalculator()
        self.cc.show()

    def refractometerCalculator(self):
        pass

    def yeastCalculator(self):
        pass

    def dilutionBoilOffCalculator(self):
        self.dbc = BoilOffDilutionCalculator()
        self.dbc.show()

    def srmCalculator(self):
        pass

    def ibuCalculator(self):
        pass

    #def hydrometerTemperatureCalculator(self):
    #    pass

    def abvCalculator(self):
        pass

    def unitConverterCalculator(self):
        self.uc = UnitConvertor()
        self.uc.show()

    def saveRecipe(self):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    recipeCreatorMain = RecipeCreatorMain()
    sys.exit(app.exec_())
