#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Calculates boil off and dilution volumes to aid in achieving gravity's.

File: BoilOffDilutionCalculator.py
Author: Adam Unger
"""

import sys
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QApplication, QMainWindow, QLineEdit, QLabel, QHBoxLayout, QVBoxLayout, QGroupBox, QPushButton
from PyQt5.QtGui import QDoubleValidator, QIcon, QFont, QDesktopServices
from PyQt5.QtCore import QUrl

class BoilOffDilutionCalculator(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.resize(600, 300)
        self.center()
        self.setWindowTitle('Boil-off and Dilution Calculator')
        self.setWindowIcon(QIcon('./Icons/dilutionBoilOffIcon.png'))

        # Create the validator for the numerical line edits.
        doubleValidator = QDoubleValidator()
        # Create bold font for the result labels
        boldFont = QFont()
        boldFont.setBold(True)

        # Create the layouts
        # main layouts
        mainLayout = QVBoxLayout()
        calculationsLayout = QHBoxLayout()
        # dilution calc's
        dilutionGroupBox = QGroupBox("Dilution Calculations")
        dilutionLayout = QVBoxLayout()
        cInitialDilutionLayout = QHBoxLayout()
        cInitialDilutionLabel = QLabel("Inital gravity:")
        self.cInitialDilutionLineEdit = QLineEdit()
        self.cInitialDilutionLineEdit.setValidator(doubleValidator)
        self.cInitialDilutionLineEdit.textEdited.connect(self.dilutionCalculation)
        cFinalDilutionLayout = QHBoxLayout()
        cFinalDilutionLabel = QLabel("Final gravity:")
        self.cFinalDilutionLineEdit = QLineEdit()
        self.cFinalDilutionLineEdit.setValidator(doubleValidator)
        self.cFinalDilutionLineEdit.textEdited.connect(self.dilutionCalculation)
        vInitialDilutionLayout = QHBoxLayout()
        vInitialDilutionLabel = QLabel("Initial volume:")
        self.vInitialDilutionLineEdit = QLineEdit()
        self.vInitialDilutionLineEdit.setValidator(doubleValidator)
        self.vInitialDilutionLineEdit.textEdited.connect(self.dilutionCalculation)
        vFinalDilutionLayout = QHBoxLayout()
        vFinalDilutionLabel = QLabel("Final volume")
        vFinalDilutionLabel.setFont(boldFont)
        self.vFinalResultDilutionLabel = QLabel()
        self.vFinalResultDilutionLabel.setFont(boldFont)
        # boil off calc's
        boilGroupBox = QGroupBox("Boil-off Calculations")
        boilLayout = QVBoxLayout()
        cInitialBoilLayout = QHBoxLayout()
        cInitialBoilLabel = QLabel("Initial gravity:")
        self.cInitialBoilLineEdit = QLineEdit()
        self.cInitialBoilLineEdit.setValidator(doubleValidator)
        self.cInitialBoilLineEdit.textEdited.connect(self.boilCalculation)
        cFinalBoilLayout = QHBoxLayout()
        cFinalBoilLabel = QLabel("Final gravity")
        cFinalBoilLabel.setFont(boldFont)
        self.cFinalResultBoilLabel = QLabel()
        self.cFinalResultBoilLabel.setFont(boldFont)
        vInitialBoilLayout = QHBoxLayout()
        vInitialBoilLabel = QLabel("Initial volume:")
        self.vInitialBoilLineEdit = QLineEdit()
        self.vInitialBoilLineEdit.setValidator(doubleValidator)
        self.vInitialBoilLineEdit.textEdited.connect(self.boilCalculation)
        vFinalBoilLayout = QHBoxLayout()
        vFinalBoilLabel = QLabel("Final volume:")
        self.vFinalBoilLineEdit = QLineEdit()
        self.vFinalBoilLineEdit.setValidator(doubleValidator)
        self.vFinalBoilLineEdit.textEdited.connect(self.boilCalculation)

        # Create the set defaults button, and a description of how these calc's were performed
        defaultsButton = QPushButton("Default", self)
        defaultsButton.setToolTip("Return all values to defaults")
        defaultsButton.clicked.connect(self.setDefaults)
        descriptionGroupBox = QGroupBox("Description")
        descriptionLabel = QLabel('These are calculated with this formula: C<sub>initial</sub>V<sub>initial</sub> = C<sub>final</sub>V<sub>final</sub>.\
                                <br>    Where:\
                                <br>C = gravity points, i.e. a SG of 1.048 = gravity points of 48\
                                <br>V = volume\
                                <br>Note that since this is a simple ratio, the units for V don\'t matter, but you must be consistent.\
                                <br>See this page for further explanation: <a href="http://beerandwinejournal.com/brew-calcs/">http://beerandwinejournal.com/brew-calcs/</a>\
                                <br><br>If the new volume is above the current volume, top off with water (only use water you know\
                                <br>is from a clean source, such as bottled spring water, water you prepared in advance).\
                                <br>If the new volume is below the current volume, boil the wort to evaporate some of the current volume.')
        descriptionLabel.setOpenExternalLinks(True)

        # Add all the layouts to each other
        # boil off calc's
        vInitialBoilLayout.addWidget(vInitialBoilLabel)
        vInitialBoilLayout.addWidget(self.vInitialBoilLineEdit)
        vFinalBoilLayout.addWidget(vFinalBoilLabel)
        vFinalBoilLayout.addWidget(self.vFinalBoilLineEdit)
        cInitialBoilLayout.addWidget(cInitialBoilLabel)
        cInitialBoilLayout.addWidget(self.cInitialBoilLineEdit)
        cFinalBoilLayout.addWidget(cFinalBoilLabel)
        cFinalBoilLayout.addWidget(self.cFinalResultBoilLabel)
        boilLayout.addLayout(vInitialBoilLayout)
        boilLayout.addLayout(vFinalBoilLayout)
        boilLayout.addLayout(cInitialBoilLayout)
        boilLayout.addLayout(cFinalBoilLayout)
        boilGroupBox.setLayout(boilLayout)
        # dilution calc's
        cInitialDilutionLayout.addWidget(cInitialDilutionLabel)
        cInitialDilutionLayout.addWidget(self.cInitialDilutionLineEdit)
        cFinalDilutionLayout.addWidget(cFinalDilutionLabel)
        cFinalDilutionLayout.addWidget(self.cFinalDilutionLineEdit)
        vInitialDilutionLayout.addWidget(vInitialDilutionLabel)
        vInitialDilutionLayout.addWidget(self.vInitialDilutionLineEdit)
        vFinalDilutionLayout.addWidget(vFinalDilutionLabel)
        vFinalDilutionLayout.addWidget(self.vFinalResultDilutionLabel)
        dilutionLayout.addLayout(cInitialDilutionLayout)
        dilutionLayout.addLayout(cFinalDilutionLayout)
        dilutionLayout.addLayout(vInitialDilutionLayout)
        dilutionLayout.addLayout(vFinalDilutionLayout)
        dilutionGroupBox.setLayout(dilutionLayout)

        # main layout's
        calculationsLayout.addWidget(boilGroupBox)
        calculationsLayout.addWidget(dilutionGroupBox)
        mainLayout.addLayout(calculationsLayout)
        mainLayout.addWidget(defaultsButton)
        mainLayout.addWidget(descriptionLabel)
        centralWidget = QWidget(self)
        centralWidget.setLayout(mainLayout)
        self.setCentralWidget(centralWidget)

        # Show the main window
        self.show()

        # Place meaningful data into the data fields
        self.setDefaults()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def gravityToPlato(self, sg):
        return (1111.14 * sg) - (630.272 * pow(sg, 2)) + (135.997 * pow(sg, 3)) - 616.868

    def platoToGravity(self, p):
        return 1.0 + (p / (258.6 - ((p / 258.2) * 227.1)))


    def setDefaults(self):
        self.cInitialDilutionLineEdit.setText(str(round(1.052, 3)))
        self.cFinalDilutionLineEdit.setText(str(round(1.05, 3)))
        self.vInitialDilutionLineEdit.setText(str(round(38.0, 2)))

        self.cInitialBoilLineEdit.setText(str(round(1.052, 3)))
        self.vInitialBoilLineEdit.setText(str(round(38.0, 2)))
        self.vFinalBoilLineEdit.setText(str(round(35.0, 2)))

        # Place meaningful data into the data fields
        self.boilCalculation()
        self.dilutionCalculation()

    def boilCalculation(self):
        ''' Calculate the final gravity. '''
        try:
            c_initial_boil = float(self.cInitialBoilLineEdit.text())
            v_initial_boil = float(self.vInitialBoilLineEdit.text())
            v_final_boil = float(self.vFinalBoilLineEdit.text())
        except ValueError:
            self.cFinalResultBoilLabel.setText('')
        else:
            cf = ((((c_initial_boil - 1) * 1000) * v_initial_boil / v_final_boil) / 1000) + 1#self.platoToGravity(self.gravityToPlato(c_initial_boil) * v_initial_boil / v_final_boil)
            self.cFinalResultBoilLabel.setText(str(round(cf, 3)))

    def dilutionCalculation(self):
        ''' Calculate the final volume. '''
        try:
            c_initial_dilution = float(self.cInitialDilutionLineEdit.text())
            v_initial_dilution = float(self.vInitialDilutionLineEdit.text())
            c_final_dilution = float(self.cFinalDilutionLineEdit.text())
        except ValueError:
            self.vFinalResultDilutionLabel.setText('')
        else:
            vf = ((c_initial_dilution - 1) * 1000) * v_initial_dilution / ((c_final_dilution - 1) * 1000)#self.gravityToPlato(c_initial_dilution) * v_initial_dilution / self.gravityToPlato(c_final_dilution)
            self.vFinalResultDilutionLabel.setText(str(round(vf, 2)))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    boilOffDilutionCalculator = BoilOffDilutionCalculator()
    sys.exit(app.exec_())
