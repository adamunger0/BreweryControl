# BreweryControl #
Fifteen gallon HERMS brewery control box design, schematics, and software.

# ArchLinux Arm Setup #
## Install python-raspberry-gpio from AUR ##
    pacman -S --needed base-devel
    mkdir gpio_tmp && cd gpio_tmp
    git clone https://aur.archlinux.org/python-raspberry-gpio.git && cd python-raspberry-gpio
    makepkg -sri && cd ../.. && rm -rf gpio_tmp

## Give regular user access to /dev/gpiomem##
This needs to be done on every reboot.
### Create gpio group ###
    groupadd gpio
### Add alarm to gpio group ###
    usermod -a -G gpio alarm
### Change owning group of /dev/gpiomem ###
    chown root.gpio /dev/gpiomem
### Change group permissions of /dev/gpiomem ###
    chmod g+rw /dev/gpiomem
### Start RPi program as alarm user ###
    su alarm
### Default permissions for /dev/gpiomem ###
    crw-------  1 root root
