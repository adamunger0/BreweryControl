#!/usr/bin/python3

'''
File: Kettle.py
Author: Adam Unger
Description: Base class for a kettle. This class can be used to represent the
             three kettles in a HERMS brew setup: HLT, MLT, and a BK.
'''

import os
import time
import logging

from PyQt5.QtCore import QTimer, pyqtSignal

from TemperatureProbe import TemperatureProbe
from Element import Element

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

class Kettle():

    # Create a signal to indicate that a temperature reading is ready. Signal:
    # (temperature, status)
    temperatureReady = pyqtSignal(float, int)

    def __init__(self, tempInput = None, elementGPIO=None, setPoint=None, setPointOn=False):
        # How often to take temperature readings. Note that the actual time will
        # vary, possibly by a lot, depending on the speed of the reading,
        # creating threads, etc; this is the interval between reads.
        self.updateTemperatureInterval = 250
        self.tempInput = tempInput # temp input name of file on system
        self.elementGPIO = elementGPIO
        self.setPoint = setPoint
        self.setPointOn = setPointOn
        self.probe = None # qthread class to take temp reading
        self.element = None
        # Keep track of temperature and status when the temperature was obtained.
        self.temp = None # degrees celsius
        self.lastStatus = None # valid previous reading or not - see the TemperatureProbe class for more info

    def startTempSens(self):
        '''
        Initiate the temperature reading process by forking a thread to take
        the actual reading. This is the entry point for taking temperature
        readings. If you would like to stop taking readings, simply set
        self.tempInput to None.
        '''
        if self.tempInput is not None:
            self.probe = TemperatureProbe(self, self.tempInput)
            self.probe.temperatureReady.connect(self.tempReady)
            self.probe.start()

    def tempReady(self, temp, status):
        '''
        This method is called when the temperature reading thread has completed.
        Note that there is very little error checking - we mostly just pass on
        the data that we've been given.
        '''
        self.lastStatus = status
        self.temp = temp
        # if the temperature couldn't be read, then set it to the setPoint so
        # that if the element is running and if it is trying to reach the
        # setpoint, then it will shut off the element. If it is running but it
        # isn't attempting to reach the setpoint then this won't affect it.
        if self.element is not None and status != TemperatureProbe.DATA_VALID:
            logger.warning('TemperatureProbe returned a None temperature:', status == TemperatureProbe.PROBE_OFFLINE_ERROR ? "probe offline error":"invalid data error")
            self.element.temp = self.setPoint
        if self.element is not None:
            self.element.temp = temp
        self.temperatureReady.emit(temp, status)
        # Restart the entire temperature reading process
        QTimer.singleShot(self.updateTemperatureInterval, self.startTempSens)

    def startElement(self):
        '''
        Start the heating element control thread.
        '''
        self.element = Element(self, self.elementGPIO, self.setPointOn, self.setPoint, self.temp)
        self.element.start()

    def stopElement(self):
        '''
        Stop the heating element control thread.
        '''
        if self.element is not None:
            self.element.stop() # stop PWM within thread and exit thread's run method
            self.element = None

    def updateSetPoint(self, newSetPoint):
        '''
        Update the set point that the element thread will target if setPointOn
        is True.
        '''
        # update the internal variable so the next element thread instantiation
        # will have the correct value
        self.setPoint = newSetPoint
        # update the element thread if one exists
        if self.element is not None:
            self.element.setPoint = newSetPoint

    def changeSetPointOn(self, on):
        '''
        Update whether the element control thread is targeting a set point or
        is simply all on, full steam ahead.
        '''
        # update the internal variable so the next element thread instantiation
        # will have the correct value
        self.setPointOn = on
        # update the element thread if one exists
        if self.element is not None:
            self.element.setPointOn = on
