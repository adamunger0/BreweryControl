#!/usr/bin/python3

"""


File: MainControlInterface.py
Author: Adam Unger
"""

import glob
import time
import logging


from PyQt5.QtCore import QThread, pyqtSignal, QTimer, QStringListModel

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

def list_probes():
    '''
    Return a list of available temperature probes. These are found in the
    /sys/bus/w1/devices folder and begin with 28. The actual file that contains
    the temperature is located inside the 28* folder and is called w1_slave.
    '''
    return glob.glob('/sys/bus/w1/devices/28*')

class UpdateInputs(QThread):
    '''
    This class simply monitors the input files and emits a signal if they've
    changed.
    '''
    # Create a signal to indicate that the inputs have changed.
    inputsChanged = pyqtSignal(list)
    inputsNotChanged = pyqtSignal()
    def __init__(self, parent, l):
        super().__init__(parent)
        self.probes = l

    def run(self):
        probesList = [p.split('/')[-1] for p in glob.glob('/sys/bus/w1/devices/28*')]
        probesList.append('<None>')
        if probesList != self.probes:
            logger.debug('new temperature probes found:' + str(probesList))
            self.inputsChanged.emit(probesList)
        else:
            self.inputsNotChanged.emit()

class ProbeList(QStringListModel):
    '''
    This will provide the underlying data model for the input comboboxes.
    '''
    def __init__(self):
        super().__init__()
        self.newInputs = False
        self.checkProbes()

    def checkProbes(self):
        self.inputsThread = UpdateInputs(self, self.stringList())
        self.inputsThread.inputsChanged.connect(self.setStringList)
        self.inputsThread.inputsNotChanged.connect(self.inputsNotChanged)
        self.inputsThread.start()

    def inputsNotChanged(self):
        QTimer.singleShot(10000, self.checkProbes)

    def setStringList(self, l):
        self.newInputs = True
        QTimer.singleShot(10000, self.checkProbes)
        super().setStringList(l)
