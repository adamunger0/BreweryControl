#!/usr/bin/python3

"""
File: MainControlInterface.py
Author: Adam Unger
"""

import sys
import time
import logging
import argparse

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import QTimer

from gauge import Gauge
from kettle import Kettle
from TemperatureProbe import TemperatureProbe, PROBE_OFFLINE_ERROR, INVALID_DATA_ERROR
from ProbeList import ProbeList
from GaugeControl import GaugeControl
from Element import Element

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
#logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)

# TODO: check for RPi library here rather than lower down - it only needs to be
# 'found' once. Checking here will save time vs checking many times in loops,
# etc in threads.....
# try to import RPi library, if it isn't found, fail gracefully
#foundRPi = True
#try:
#    import RPi.GPIO as GPIO
#except ModuleNotFoundError as e:
#    logger.warning('ModuleNotFoundError: No module named \'RPi\': unable to import RPi library. Is this machine a RPi?')
#    foundRPi = False

class MainControlInterface(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def closeEvent(self, event):
        try:
            import RPi.GPIO as GPIO
            GPIO.cleanup()
            logger.debug('Cleaned up gpio\'s with RPi.GPIO.cleanup()')
        except ModuleNotFoundError as e:
            logger.warning('ModuleNotFoundError: No module named \'RPi\': unable to cleanup GPIO pins with GPIO.cleanup(). Is this machine an RPi?')
        if self.gaugeControl1 is not None:
            self.gaugeControl1.finish()
        if self.gaugeControl2 is not None:
            self.gaugeControl2.finish()
        if self.gaugeControl3 is not None:
            self.gaugeControl3.finish()

    def initUI(self):
        # Load the main UI
        uic.loadUi('MainControlInterface.ui', self)

        #self.showFullScreen()

        self.gaugeControl1 = GaugeControl(None)
        #self.gaugeControl1.horizontalLayout.setHidden(True)
        #self.gaugeControl1.horizontalLayout_2.setHidden(True)
        self.gaugeControl2 = GaugeControl(13)
        self.gaugeControl3 = GaugeControl(15)
        gaugesLayout = self.gaugeControls
        gaugesLayout.addWidget(self.gaugeControl1)
        gaugesLayout.addWidget(self.gaugeControl2)
        gaugesLayout.addWidget(self.gaugeControl3)

        # Create the unit combo box
        self.tempUnitsComboBox.addItems([str(u"\N{DEGREE SIGN}") + "C", str(u"\N{DEGREE SIGN}") + "F"])
        self.tempUnitsComboBox.currentIndexChanged.connect(self.tempUnitsChanged)

        self.statusBar().showMessage('Ready')

        # Show the main window
        self.show()

    def tempUnitsChanged(self):
        if self.tempUnitsComboBox.currentText() == str(u"\N{DEGREE SIGN}") + "C":
            self.gaugeControl1.tempUnitsChanged(GaugeControl.CELSIUS)
            self.gaugeControl2.tempUnitsChanged(GaugeControl.CELSIUS)
            self.gaugeControl3.tempUnitsChanged(GaugeControl.CELSIUS)
        else:
            self.gaugeControl1.tempUnitsChanged(GaugeControl.FAHRENHEIT)
            self.gaugeControl2.tempUnitsChanged(GaugeControl.FAHRENHEIT)
            self.gaugeControl3.tempUnitsChanged(GaugeControl.FAHRENHEIT)


if __name__ == '__main__':
    # define the logging output levels
    LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}
    # create the parser to parse the user's input
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", help="verbosity of logging output: debug, info, warning, error, critical")
    args = parser.parse_args()
    level = logging.WARNING # default if no level is specified in basic
    level = LEVELS.get(str(args.verbosity).lower(), logging.NOTSET)
    # create and configure the logging module
    logging.basicConfig(level=level, format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')

    # create the application and run it
    app = QApplication(sys.argv)
    mainControlInterface = MainControlInterface()
    # Start the main event loop and exit when finished
    sys.exit(app.exec_())
