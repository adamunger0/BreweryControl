#!/usr/bin/python3

'''
File: HERMSLogger.py
Author: Adam Unger
Description: This provides logging functionality for the HERMS system. The
    output files are intended to be both machine and human readable.
'''

import json
import logging
import time
import os

from PyQt5.QtCore import QThread

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

# Version of this JSONLogger. When there are versions of this logger out in
# the wild, parsing programs can use this number to determine what format
# the data is in.
JSON_PARSER_VERSION = 1

class HERMSLogger(QThread):
    '''
    Will log HERMS messages to a file in JSON format. This will create a new
    directory each time it is run. There's nothing saying that you can't merge
    these directories, but the idea is to have a single HERMSLogger object
    throughout the brew cycle so that the files in the directory correspond to a
    single brewing session. Each file will be titled with the timestamp of it's
    creation.
    '''
    def __init__(self, path):
        self.dirname = path + os.sep + str(time.time()).replace('.', '_')
        os.mkdir(self.dirname)

    def run(self, bktemp, hlttemp, mlttemp, bksp, hltsp, mltsp, bkp, bki, bkd, hp, hi, hd):
        # create a default json log entry that doesn't yet have any readings
        jsonEntry = {'Log version':JSON_PARSER_VERSION,
                    'Notes':'This file name and all timestamps are UNIX epoch. All temperatures are degrees celsius.',
                    'BK Temp':bktemp,
                    'HLT Temp':hlttemp,
                    'MLT Temp':mlttemp,
                    'BK Setpoint':bksp,
                    'HLT Setpoint':hltsp,
                    'MLT Setpoint':mlttemp,
                    'BK P Coefficient':bkp,
                    'BK I Coefficient':bki,
                    'BK D Coefficient':bkd,
                    'HLT P Coefficient':hp,
                    'HLT I Coefficient':hi,
                    'HLT D Coefficient':hd}

        # create a unique filename with the UNIX epoch timestamp
        filename = self.dirname + os.sep + str(time.time()).replace('.', '_')
        try:
            with open(filename, 'w') as fp:
                json.dump(jsonEntry, fp, indent=2, sort_keys=True)
        except:
            logger.error('unable to open "%s" for writing HERMS data', filename)
