#!/usr/bin/python3

"""
File: Element.py
Author: Adam Unger
Description: This module implements a PID controller for a heater element.
             Connect a heater element to a relay or other switch. This will
             ultimately simply toggle a GPIO using PWM.
"""

import os
import time
import logging

from PyQt5.QtCore import QThread, pyqtSignal
#import RPi.GPIO as GPIO

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

# try to import RPi library, if it isn't found, fail gracefully
foundRPi = True
try:
    import RPi.GPIO as GPIO
except ModuleNotFoundError as e:
    logger.warning('ModuleNotFoundError: No module named \'RPi\': unable to import RPi library. Is this machine a RPi?')
    foundRPi = False

from PID import PID


class Element(QThread):

    indicatorToggle = pyqtSignal(bool)

    def __init__(self, parent, hot, setPointOn, setPoint, temp):
        super().__init__(parent) # parent is necessary for graceful cleanup/exits of this thread
        self.hot = hot # pin used to control the "hot" wire SSR
        self.setPointOn = setPointOn
        self.setPoint = setPoint
        self.pwm = None
        self.finishNow = False
        self.temp = temp

    def stop(self):
        self.finishNow = True

    def run(self):
        if not foundRPi or self.hot is None:
            return
        logger.info('starting element control')
        GPIO.setwarnings(False) # we don't care about previous exits from RPi.GPIO that may have gone awry
        GPIO.setmode(GPIO.BOARD) # Set the GPIO library to use the board pin numbering
        GPIO.setup(self.hot, GPIO.OUT, initial=GPIO.LOW) # Set pin as a pull down output (for safety; the SSR's are active high)
        self.pwm = GPIO.PWM(self.hot, 100) # Create a PWM object with the pin and at 100Hz
        self.pwm.start(0) # start the PWM on the other leg; start at 0 (off) for safety
        # Now enter main event loop. The PWM is now on, this loop will simply monitor the variables
        # and adjust as needed.
        while not self.finishNow:
            if self.setPointOn:
                # TODO: only update the PWM's duty cycle if it's changed
                # Too High KP will lead to oscillation in values and will tend to
                # generate an offset.
                # KI will counteract the offset. Higher Value of KI implies
                # that the Setpoint will reach the PV too fast. If this
                # action is very fast, the process variable is prone to be
                # unsteady.
                # KD keeps this under control.
                pid = PID(50, 0.0, 0, setpoint=self.setPoint, output_limits=(0, 100))#, proportional_on_measurement=True)
                dutyCycle = None
                while not self.finishNow and self.setPointOn:
                    pid.setpoint = self.setPoint # get updated setpoint in case it has changed since the last iteration
                    dutyCycle = pid(self.temp)
                    #if power is not None:
                        #if power != dutyCycle:
                        #dutyCycle = power
                    self.pwm.ChangeDutyCycle(dutyCycle)
                    #else: # safe default
                    #    self.pwm.ChangeDutyCycle(0)
                        #self.indicatorToggle.emit(False)
                    logger.info('PID output(0-100): %s, currentTemp: %s%s, setPoint: %s%s', str(dutyCycle), str(self.temp), str(u" \N{DEGREE SIGN}") + "C", str(self.setPoint), str(u" \N{DEGREE SIGN}") + "C")
            else:
                self.pwm.ChangeDutyCycle(100)
                #self.indicatorToggle.emit(True)
        #self.indicatorToggle.emit(False)
        self.pwm.stop() # shutoff the PWM hot leg
