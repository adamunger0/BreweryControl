#!/usr/bin/python3

"""
File: GaugeControl.py
Author: Adam Unger
"""

import os
import time
import logging

from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, QTimer, Qt
from PyQt5.QtWidgets import QWidget

from ProbeList import ProbeList
from TemperatureProbe import TemperatureProbe
from TemperatureProbe import DATA_VALID
from TemperatureProbe import INVALID_DATA_ERROR
from TemperatureProbe import PROBE_OFFLINE_ERROR
from gauge import Gauge
from Element import Element

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)
#logger.setLevel(logging.WARNING)

# try to import RPi library, if it isn't found, fail gracefully
foundRPi = True
try:
    import RPi.GPIO as GPIO
except ModuleNotFoundError as e:
    logger.warning('ModuleNotFoundError: No module named \'RPi\': unable to import RPi library. Is this machine an RPi?')
    foundRPi = False

class GaugeControl(QWidget):

    CELSIUS = 1
    FAHRENHEIT = 2

    def __init__(self, gpio):
        super().__init__()
        self.elementGPIO = gpio
        # Default to setting the pin low to ensure that we don't accidentally
        # turn the element on before it should (improper exit from previous run, etc)
        if foundRPi:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BOARD) # Set the GPIO library to use the board pin numbering
            if self.elementGPIO is not None:
                GPIO.setup(gpio, GPIO.OUT, initial=GPIO.LOW) # Set pin as a pull down output (for safety; the SSR's are active high)
            #GPIO.output(gpio, False)
        self.initUI()


    def initUI(self):
        # Load the main UI
        uic.loadUi('GaugeControl.ui', self)

        # How often to take temperature readings. Note that the actual time will
        # vary, possibly by a lot, depending on the speed of the reading,
        # creating threads, etc; this is the interval between reads.
        self.updateTemperatureInterval = 250

        # Create the element variable that will turn the elements on/off as needed
        self.element = None

        # Create the element control timer - default is 0min 0sec, i.e. None
        self.elementControlTimer = None

        # Create the probe list fetcher variable
        self.probe = None

        # Internal temperature variables are always in celsius. We need to store
        # them here so that we can retain the decimal precision even after we
        # round them for display.
        self.temp = 0.0
        self.setPoint = 0.0
        self.input = '<None>'
        self.tempUnits = self.CELSIUS
        self.setPointOn = False

        # Set the gauge parameters so that they don't need to be calculated on
        # every switch between celsius and fahrenheit.
        self.cStart = 0.0
        self.cEnd = 110.0
        self.cLabelInterval = 5
        self.cMajorInterval = 5
        self.cMinorInterval = 1
        self.fStart = 30.0
        self.fEnd = 230.0
        self.fLabelInterval = 10
        self.fMajorInterval = 10
        self.fMinorInterval = 2

        # create the gauge and it's parameters
        self.gaugeWidget = Gauge()
        self.gaugeWidget.setScaleStart(self.cStart)
        self.gaugeWidget.setScaleEnd(self.cEnd)
        self.gaugeWidget.setLabelInterval(self.cLabelInterval)
        self.gaugeWidget.setMajorInterval(self.cMajorInterval)
        self.gaugeWidget.setMinorInterval(self.cMinorInterval)

        self.gaugeControlLayout.insertWidget(0, self.gaugeWidget, 1)

        self.tempUnitsLabel.setText(str(u"\N{DEGREE SIGN}") + "C")

        # Create the input combo box
        self.inputComboBox.setModel(ProbeList())
        self.inputComboBox.addItem('<None>')
        self.inputComboBox.setCurrentIndex(0)
        self.inputComboBox.currentIndexChanged.connect(self.inputChanged)

        # Create the set point spin box
        self.setPointSpinner.valueChanged.connect(self.setPointChanged)
        self.setPointSpinner.setSuffix(str(u" \N{DEGREE SIGN}") + "C")

        # Setup the element indicator. There are 2 possible states: on or off.
        # Red indicates the element is on, and green indicates that it is off.
        self.elementIndicatorButton.setStyleSheet('background-color: green; color: black')
        self.elementIndicatorButton.setText('Element Off')

        # Setup the toggled signal from the set point checkbox
        self.setPointCheckbox.stateChanged.connect(self.setPointToggled)

        # Setup the element checkbox
        self.elementControlCheckbox.stateChanged.connect(self.elementControlToggled)

        # Setup the element timer spinners
        self.elementTimerMinSpinner.valueChanged.connect(self.elementControlTimeChanged)
        self.elementTimerSecSpinner.valueChanged.connect(self.elementControlTimeChanged)

        # Start taking temperature readings at timed intervals
        self.getTemperatureReading()

    def finish(self):
        if self.probe is not None:
            self.probe.quit()
        if self.element is not None:
            self.element.stop()

    def getTemperatureReading(self):
        self.startTime = time.perf_counter()
        if self.input == '<None>':
            #self.updateGauge(0.0)
            self.updateGauge(self.gaugeWidget.getScaleStart(), DATA_VALID)
        else:
            self.probe = TemperatureProbe(self, self.input)
            self.probe.temperatureReady.connect(self.updateGauge)
            self.probe.start()

    def updateGauge(self, temp, status):
        # catch temperature read errors first
        if status == INVALID_DATA_ERROR:
            logger.warning('tempurature probe on pin %s returned invalid data', str(self.elementGPIO))
            return
        elif status == PROBE_OFFLINE_ERROR:
            logger.warning('tempurature probe on pin %s is offline', str(self.elementGPIO))
            return

        # if we're here, then we presumably have valid data - display it
        self.temp = temp
        if self.tempUnits == self.FAHRENHEIT:
            temp = self.celsiusToFahrenheit(temp)
        self.gaugeWidget.setNeedlePosition(temp)
        self.tempLabel.setText(self.makeLabel(round(temp, 1), 1))
        if self.element is not None:
            logger.info('updating element temp to %s%s', str(self.temp), str(u" \N{DEGREE SIGN}") + "C")
            self.element.temp = self.temp
        logger.debug('Temp update took %sms', str((time.perf_counter() - self.startTime) * 1000))

        # Restart the entire process
        QTimer.singleShot(self.updateTemperatureInterval, self.getTemperatureReading)

    def toggleIndicator(self, state):
        if state:
            self.elementIndicatorButton.setStyleSheet('background-color: red; color: black')
            self.elementIndicatorButton.setText('Element On')
        else:
            self.elementIndicatorButton.setStyleSheet('background-color: green; color: black')
            self.elementIndicatorButton.setText('Element Off')

    def restartGaugeTimer(self, e):
        logger.warning('Temp update failed in %sms with %s error code', str((time.perf_counter() - self.startTime) * 1000), e)
        # TODO: handle e i.e. PROBE_OFFLINE_ERROR and INVALID_DATA_ERROR
        # Restart the entire process
        QTimer.singleShot(self.updateTemperatureInterval, self.getTemperatureReading)

    def celsiusToFahrenheit(self, c):
        return c * 1.8 + 32

    def fahrenheitToCelsius(self, f):
        return (f - 32) / 1.8

    def makeLabel(self, number, numDecimals):
        '''
        Convert the current number for the label into a proper label for the
        number of specified decimal places.
        '''
        numList = str(number).split('.')
        l = str(numList[0])
        if len(numList) > 1 and numDecimals > 0:
            l += '.' # since we want a decimal'd number, start by appending a decimal
            n = 0
            lenn = len(numList[1]) # number of decimals in given number
            # append each digit to the end of the number
            while n < numDecimals:
                if n < lenn: # get a digit from the given number since one exists
                    l += numList[1][n]
                else: # the given number didn't have this many decimals so append a 0 instead.
                    l += '0'
                n += 1
        return l

    def inputChanged(self):
        '''
        The underlying input list has either changed or a new selection has been
        made by the user. Here we will make sure the combo box and internal
        variables are updated.
        '''
        # This slot was called because the underlying dataset of the comboboxes
        # has changed. This means that we need to reset the selected input.
        if self.inputComboBox.model().newInputs:
            self.inputComboBox.model().newInputs = False
            index = self.inputComboBox.findText(self.input)
            self.inputComboBox.setCurrentIndex(index if index >= 0 else self.inputComboBox.findText('<None>'))
            self.input = '<None>' if index < 0 else self.input
        # This slot was called because the user made a selection. This means
        # that we need to get the selected input.
        else:
            self.input = self.inputComboBox.currentText()

        if self.input == '<None>':
            self.elementControlCheckbox.setChecked(False)
            if self.element is not None:
                logger.info('turning element off')
                self.element.stop() # stop PWM within thread and exit thread's run method
                self.element = None

    def setPointChanged(self, value):
        if self.tempUnits == self.CELSIUS:
            self.setPoint = value
        else:
            self.setPoint = self.fahrenheitToCelsius(value)
        logger.info('set point changed to %s%s', self.setPoint, str(u"\N{DEGREE SIGN}") + "C")
        if self.element is not None:
            self.element.setPoint = self.setPoint

    def tempUnitsChanged(self, units):
        # convert the text in the line edit to the appropriate units and update our internal variables
        if units == self.CELSIUS: # change enverything to celsius
            logger.info('units changed to celsius')
            self.changeToCelsius()
        else: # change everything to fahrenheit
            logger.info('units changed to fahrenheit')
            self.changeToFahrenheit()

    def setPointToggled(self, state):
        if state == Qt.CheckState.Unchecked:
            logger.info('set point off')
            self.setPointOn = False
            if self.element is not None:
                self.element.setPointOn = False
        elif state == Qt.CheckState.Checked:
            logger.info('set point on')
            self.setPointOn = True
            if self.element is not None:
                self.element.setPoint = self.setPoint
                self.element.setPointOn = True

    def elementControlToggled(self, state):
        # TODO: make this only start an element if an input temperature reading is selected
        if state == Qt.CheckState.Unchecked:
            logger.info('turning element off')
            if self.input is not None:
                if self.element is not None:
                    self.element.stop() # stop PWM within thread and exit thread's run method
                self.element = None
        elif state == Qt.CheckState.Checked:
            #if self.input != '<None>':
            logger.info('turning element on')
            self.element = Element(self, self.elementGPIO, self.setPointOn, self.setPoint, self.temp)
            self.element.indicatorToggle.connect(self.toggleIndicator)
            self.element.start()
            #else:
            #    self.elementControlCheckbox.setChecked(False)

    def elementControlTimeChanged(self):
        m = self.elementTimerMinSpinner.value()
        s = self.elementTimerSecSpinner.value()
        if m == 0 and s == 0:
            self.elementControlTimer = None
        logger.info('element timer: %i min and %i sec', m, s)
        ms = ((m * 60) + s) * 1000 # convert to milliseconds

    def changeToCelsius(self):
        self.tempUnits = self.CELSIUS
        self.tempLabel.setText(self.makeLabel(round(self.temp, 1), 1)) # temperature display
        self.setPointSpinner.setValue(float(self.makeLabel(round(self.setPoint, 1), 1))) # set point

        self.gaugeWidget.setScaleStart(self.cStart)
        self.gaugeWidget.setScaleEnd(self.cEnd)
        self.gaugeWidget.setLabelInterval(self.cLabelInterval)
        self.gaugeWidget.setMajorInterval(self.cMajorInterval)
        self.gaugeWidget.setMinorInterval(self.cMinorInterval)
        self.gaugeWidget.setNeedlePosition(self.temp)

        self.tempUnitsLabel.setText(str(u"\N{DEGREE SIGN}") + "C")
        self.setPointSpinner.setSuffix(str(u" \N{DEGREE SIGN}") + "C")

    def changeToFahrenheit(self):
        self.tempUnits = self.FAHRENHEIT
        tf = self.celsiusToFahrenheit(self.temp)
        spf = self.celsiusToFahrenheit(self.setPoint)

        self.tempLabel.setText(self.makeLabel(round(tf, 1), 1)) # temperature display
        self.setPointSpinner.setValue(float(self.makeLabel(round(spf, 1), 1))) # set point

        self.gaugeWidget.setScaleStart(self.fStart)
        self.gaugeWidget.setScaleEnd(self.fEnd)
        self.gaugeWidget.setLabelInterval(self.fLabelInterval)
        self.gaugeWidget.setMajorInterval(self.fMajorInterval)
        self.gaugeWidget.setMinorInterval(self.fMinorInterval)
        self.gaugeWidget.setNeedlePosition(tf)

        self.tempUnitsLabel.setText(str(u"\N{DEGREE SIGN}") + "F")
        self.setPointSpinner.setSuffix(str(u" \N{DEGREE SIGN}") + "F")
