#!/usr/bin/python3

"""


File: TemperatureProbe.py
Author: Adam Unger
"""

import os
import time
import logging

from PyQt5.QtCore import QThread, pyqtSignal

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

# NOTE: to view maximum number of threads available, import QThreadPool, create
# a QThreadPool object, and call it's maxThreadCount() method

# NOTE: see doc.qt.io/qt-5/threads-technologies.html for Qt5 multithreading
# options, also see martinfitzpatrick.name/article/multithreading-pyqt-
# applications-with-qthreadpool/

# NOTE: see pimylifeup.com/raspberry-pi-temperature-sensor/ for decent
# explanation of getting the temperature probes working

# Tell the kernel to load the appropriate modules for using 1-wire with
# a temperature device. These calls will throw something like "modprobe: ERROR:
# could not insert 'w1_gpio': Operation not permitted" if there isn't any
# support for them.
#os.system('modprobe w1-gpio')
#os.system('modprobe w1-therm')

DATA_VALID = 0
PROBE_OFFLINE_ERROR = 1
INVALID_DATA_ERROR = 2

class TemperatureProbe(QThread):

    # Create a signal to indicate that a temperature reading is ready. Signal:
    # (temperature, status)
    temperatureReady = pyqtSignal(float, int)

    def __init__(self, parent, probe):
        super().__init__(parent) # parent is necessary for graceful cleanup/exits of this thread

        # This is the location on the file system where the probe directory's are.
        self.pathToProbes = '/sys/bus/w1/devices/'
        # This is the name of the file that contains the device information.
        self.deviceFile = '/w1_slave'
        # The device that we want to read data from.
        self.probe = probe

    def _readTempRaw(self):
        '''
        Get the data from the input probe's data file. No data manipulation
        happens here - only file open, read, and close.
        '''
        try:
            f = open(self.pathToProbes + self.probe + self.deviceFile, 'r')
            f.seek(0)
            lines = f.readlines()
            f.close()
            return lines
        except FileNotFoundError:
            logger.warning('unable to parse %s:', self.deviceFile, exc_info=True)
            return []

    def run(self):
        '''
        Read the temperature from the input probe and return as a float in
        degrees Celsius.
        '''
        lines = self._readTempRaw()
        if len(lines) > 0 and lines[0].strip()[-3:] == 'YES':
            equals_pos = lines[1].find('t=')
            if equals_pos != -1:
                temp_string = lines[1][equals_pos + 2:]
                self.temperatureReady.emit(float(temp_string) / 1000.0, DATA_VALID)
                logger.debug('retrieved temperature data for ' + self.probe + ': ' + str(float(temp_string) / 1000.0) + str(u"\N{DEGREE SIGN}") + "C")
                return
            logger.warning('unable to retrieve temperature data for ' + self.probe + ': ' + 'probe is sending junk or file is corrupted')
            self.temperatureReady.emit(0.0, INVALID_DATA_ERROR)
            return
        logger.warning('unable to retrieve temperature data for ' + self.probe + ': ' + 'probe is offline')
        self.temperatureReady.emit(0.0, PROBE_OFFLINE_ERROR)
