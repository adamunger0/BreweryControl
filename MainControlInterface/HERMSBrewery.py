#!/usr/bin/python3

'''
File: HERMSBrewery.py
Author: Adam Unger
Description: This class implements and controls three brew Kettles: MLT, HLT, BK. Everything that is done is logged according to the logging parameters given.
'''

import os
import time
import logging
from pathlib import Path

from PyQt5.QtCore import QTimer, pyqtSignal

from Kettle import Kettle
from HERMSLogger import HERMSLogger

# Setup the logging module; CRITICAL, ERROR, WARNING, INFO, DEBUG
logger = logging.getLogger(__name__)

class HERMSBrewery():
    def __init__(self):
        # initialize the HERMS logger
        self.hermsLogger = HERMSLogger(str(Path.home()) + os.sep + '.brewery')

        # initialize the kettle's
        self.HLT = Kettle()
        self.MLT = Kettle()
        self.BK = Kettle()
