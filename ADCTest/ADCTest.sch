EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "MAX1607 ADC Test"
Date "2018-04-30"
Rev "1"
Comp ""
Comment1 "Small test board to test functionality of ADC with current transformer."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transformer_1P_1S T1
U 1 1 5AE7F519
P 3550 2800
F 0 "T1" H 3550 3050 50  0000 C CNN
F 1 "CSE187-L" H 3550 2500 50  0000 C CNN
F 2 "" H 3550 2800 50  0001 C CNN
F 3 "" H 3550 2800 50  0001 C CNN
	1    3550 2800
	1    0    0    -1  
$EndComp
$Comp
L D_Bridge_-AA+ D1
U 1 1 5AE7F660
P 4750 2800
F 0 "D1" H 4800 3075 50  0000 L CNN
F 1 "BAS4002A-RPP" H 4800 3000 50  0000 L CNN
F 2 "" H 4750 2800 50  0001 C CNN
F 3 "" H 4750 2800 50  0001 C CNN
	1    4750 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2600 4200 2600
Wire Wire Line
	4200 2600 4200 2400
Wire Wire Line
	4200 2400 4750 2400
Wire Wire Line
	4750 2400 4750 2500
Wire Wire Line
	4750 3100 4750 3200
Wire Wire Line
	4750 3200 4200 3200
Wire Wire Line
	4200 3200 4200 3000
Wire Wire Line
	4200 3000 3950 3000
$EndSCHEMATC
